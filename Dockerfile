FROM microsoft/dotnet:2.1-sdk AS builder
WORKDIR /sln

# Copy csproj and restore as distinct layers
COPY ./ ./
RUN dotnet restore

COPY ./ ./
RUN dotnet build -c Release --no-restore

RUN dotnet publish "./SALON_HAIR_API/SALON_HAIR_API.csproj" -c Release -o "../dist" --no-restore

# Build runtime image
FROM microsoft/dotnet:2.1-aspnetcore-runtime as runtime
MAINTAINER Jacob Pham <dieupx@ctnet.vn>
ENV TZ Asia/Ho_Chi_Minh
ENV HOST 0.0.0.0
ENV ASPNETCORE_ENVIRONMENT Production


WORKDIR /app
COPY --from=builder /sln/dist .
ENTRYPOINT ["dotnet", "SALON_HAIR_API.dll"]