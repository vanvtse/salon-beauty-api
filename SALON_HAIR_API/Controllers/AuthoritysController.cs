
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using ULTIL_HELPER;
using Microsoft.AspNetCore.Authorization;
using SALON_HAIR_API.Exceptions;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SALON_HAIR_API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]

    public class AuthoritysController : CustomControllerBase
    {
        private readonly IAuthority _authority;
        private readonly IUser _user;
        

        public AuthoritysController(IAuthority authority, IUser user)
        {
           
            _authority = authority;
            _user = user;
        }

        // GET: api/Authoritys
        [HttpGet]
        public IActionResult GetAuthority(int page = 1, int rowPerPage = 50, string keyword = "", string orderBy = "", string orderType = "")
        {
            
            var data = _authority.SearchAllFileds(keyword);
            data = GetByCurrentSalon(data);                
            var dataReturn = _authority.LoadAllCollecttion(data);

            return OkList(dataReturn);
        }
        // GET: api/Authoritys/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAuthority([FromRoute] long id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var authority = await _authority.FindAsync(id);

                if (authority == null)
                {
                    return NotFound();
                }
                return Ok(authority);
            }
            catch (Exception e)
            {

                  throw new UnexpectedException(id, e);
            }
        }
        // PUT: api/Authoritys/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthority([FromRoute] long id, [FromBody] Authority authority)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != authority.Id)
            {
                return BadRequest();
            }
            try
            {
             
                await _authority.EditAsync(authority);
                return CreatedAtAction("GetAuthority", new { id = authority.Id }, authority);
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!AuthorityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }           
            catch (Exception e)
            {

                  throw new UnexpectedException(authority,e);
            }
        }
        [HttpGet("testparse")]
        public async Task<IActionResult> PutAuthorityaaaaa(string date)
        {
            try
            {
                //var datetime = GetDateRangeQuery(date);

                //DateTime utcDate1 = datetime.ToUniversalTime();
                //DateTime date2 = datetime.ToLocalTime();
                ////DateTime tstTime = TimeZoneInfo.ConvertTime(datetime, TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time"));
                //var zone = TimeZoneInfo.Local; // For example
                //Console.WriteLine(zone.DisplayName);
                //return Ok(new { utcDate1, date2, zone, zone.DisplayName });

              await LogAsyncxx(new
                {
                    date
                }, "wwwroot", "request");

                return Ok("clmm"+date);
            }
            catch (Exception e)
            {

                throw e;
            }

        }
        // POST: api/Authoritys
        [HttpPost]
        public async Task<IActionResult> PostAuthority([FromBody] Authority authority)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            
                await _authority.AddAsync(authority);
                return CreatedAtAction("GetAuthority", new { id = authority.Id }, authority);
            }
            catch (Exception e)
            {
                throw new UnexpectedException(authority,e);
            }
          
        }
        // DELETE: api/Authoritys/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAuthority([FromRoute] long id)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var authority = await _authority.FindAsync(id);
                if (authority == null)
                {
                    return NotFound();
                }

                await _authority.DeleteAsync(authority);

                return Ok(authority);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(id,e);
            }
          
        }
        private bool AuthorityExists(long id)
        {
            return _authority.Any<Authority>(e => e.Id == id);
        }
        //private IQueryable<Authority> GetByCurrentSpaBranch(IQueryable<Authority> data)
        //{
        //    var currentSalonBranch = _user.Find(JwtHelper.GetIdFromToken(User.Claims)).SalonBranchCurrentId;

        //    if (currentSalonBranch != default || currentSalonBranch != 0)
        //    {
        //        data = data.Where(e => e.SalonBranchId == currentSalonBranch);
        //    }
        //    return data;
        //}
        private IQueryable<Authority> GetByCurrentSalon(IQueryable<Authority> data)
        {
            var salonId = JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));
            data = data.Where(e => e.SalonId == salonId);
            return data;
        }
        private DateTime GetDateRangeQuery(string date)
        {
            date += "";

            var st = DateTime.Now;
            if (!string.IsNullOrEmpty(date))
            {
                st = DateTime.Parse(date);
            }

            return st;
        }
        private async Task LogAsyncxx(object logMessage, string path, string type = "info")
        {
            try
            {
                await LogAsync(JsonConvert.SerializeObject(logMessage,
          new JsonSerializerSettings
          {
              ContractResolver = new CamelCasePropertyNamesContractResolver()
          }), path, type);
            }
            catch (Exception ex)
            {

                throw ex;
            }
          
        }
        private async Task LogAsync(string logMessage, string root, string type = "info")
        {
            try
            {
                string strLogMessage = string.Empty;
                String fName = DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
                StreamWriter swLog;

                strLogMessage = string.Format("{0}: {1}", DateTime.Now, logMessage);
                var current = Directory.GetCurrentDirectory();
                var originalDirectory = new DirectoryInfo($"{current}\\{root}\\Logs\\{type}\\");
                string pathString = Path.Combine(originalDirectory.ToString(), DateTime.Now.ToString("yyyy-MM"));
                //var fileName1 = Path.GetFileName(fName);
                //create directory if does not exist
                bool isExists = Directory.Exists(pathString);
                if (!isExists) Directory.CreateDirectory(pathString);
                //generate local path to save file
                var path = $"{pathString}\\{fName}";
                if (!System.IO.File.Exists(path))
                {

                    FileStream fs = new FileStream(path, FileMode.CreateNew);
                    StreamWriter swr = new StreamWriter(fs);
                    try
                    {
                        swr.Close();
                        fs.Close();
                    }
                    catch (Exception ex)
                    {
                        swr.Close();
                        fs.Close();
                    }
                }

                if (!System.IO.File.Exists(path))
                {
                    swLog = new StreamWriter(path);
                }
                else
                {
                    swLog = System.IO.File.AppendText(path);
                }

                await swLog.WriteLineAsync(strLogMessage);
                await swLog.WriteLineAsync();
                swLog.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}

