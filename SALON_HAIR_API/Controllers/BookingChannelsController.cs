﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using ULTIL_HELPER;
using Microsoft.AspNetCore.Authorization;
using SALON_HAIR_API.Exceptions;
using System.Collections.Generic;

namespace SALON_HAIR_API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class BookingChannelsController : CustomControllerBase
    {
        private readonly IBookingChannel _BookingChannel;
        private readonly IUser _user;

        public BookingChannelsController(IBookingChannel BookingChannel, IUser user)
        {
            _BookingChannel = BookingChannel;
            _user = user;
        }

        // GET: api/BookingChannels
        [HttpGet]
        public IActionResult GetBookingChannel(int page = 1, int rowPerPage = 50, string keyword = "", string orderBy = "", string orderType = "",string status ="")
        {
            var data = _BookingChannel.SearchAllFileds(keyword);
            data = GetByCurrentSalon(data);
            data = string.IsNullOrEmpty(status) ? data : data.Where(e => e.Status.Equals(status));
            var dataReturn = _BookingChannel.LoadAllInclude(data);
            return OkList(dataReturn);
        }
        // GET: api/BookingChannels/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBookingChannel([FromRoute] long id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var BookingChannel = await _BookingChannel.FindAsync(id);

                if (BookingChannel == null)
                {
                    return NotFound();
                }
                return Ok(BookingChannel);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(id, e);
            }
        }

        // PUT: api/BookingChannels/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBookingChannel([FromRoute] long id, [FromBody] BookingChannel BookingChannel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != BookingChannel.Id)
            {
                return BadRequest();
            }
            try
            {
                BookingChannel.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                var datas = new List<BookingChannel>();
                datas.Add(BookingChannel);
                if (BookingChannel.IsDefault == true)
                {
                    var dataReturn = _BookingChannel.GetAll().Where(e => e.IsDefault == true).Where(e=>e.Id!=BookingChannel.Id).ToList();
                    dataReturn.ForEach(e => { e.IsDefault = false; });
                    datas.AddRange(dataReturn);
                }

                await _BookingChannel.EditRangeAsync(datas);
                //return CreatedAtAction("GetBookingChannel", new { id = BookingChannel.Id }, BookingChannel);
                return OkList(datas);
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!BookingChannelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (Exception e)
            {

                throw new UnexpectedException(BookingChannel, e);
            }
        }

        // POST: api/BookingChannels
        [HttpPost]
        public async Task<IActionResult> PostBookingChannel([FromBody] BookingChannel BookingChannel)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                BookingChannel.CreatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                await _BookingChannel.AddAsync(BookingChannel);
                return CreatedAtAction("GetBookingChannel", new { id = BookingChannel.Id }, BookingChannel);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(BookingChannel, e);
            }

        }

        // DELETE: api/BookingChannels/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBookingChannel([FromRoute] long id)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var BookingChannel = await _BookingChannel.FindAsync(id);
                if (BookingChannel == null)
                {
                    return NotFound();
                }

                await _BookingChannel.DeleteAsync(BookingChannel);

                return Ok(BookingChannel);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(id, e);
            }

        }

        private bool BookingChannelExists(long id)
        {
            return _BookingChannel.Any<BookingChannel>(e => e.Id == id);
        }
        //private IQueryable<Authority> GetByCurrentSpaBranch(IQueryable<Authority> data)
        //{
        //    var currentSalonBranch = _user.Find(JwtHelper.GetIdFromToken(User.Claims)).SalonBranchCurrentId;

        //    if (currentSalonBranch != default || currentSalonBranch != 0)
        //    {
        //        data = data.Where(e => e.SalonBranchId == currentSalonBranch);
        //    }
        //    return data;
        //}
        private IQueryable<BookingChannel> GetByCurrentSalon(IQueryable<BookingChannel> data)
        {
            var salonId = JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));
            data = data.Where(e => e.SalonId == salonId);
            return data;
        }
    }
}

