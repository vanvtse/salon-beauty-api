
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using ULTIL_HELPER;
using Microsoft.AspNetCore.Authorization;
using SALON_HAIR_API.Exceptions;
namespace SALON_HAIR_API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class CustomerPackageTransactionsController : CustomControllerBase
    {
        private readonly ICustomerPackageTransaction _customerPackageTransaction;
        private readonly IUser _user;

        public CustomerPackageTransactionsController(ICustomerPackageTransaction customerPackageTransaction, IUser user)
        {
            _customerPackageTransaction = customerPackageTransaction;
            _user = user;
        }

        // GET: api/CustomerPackageTransactions
        [HttpGet]
        public IActionResult GetCustomerPackageTransaction(long customerId, long packageId,int day,int month,int year, int page = 1, int rowPerPage = 50, string keyword = "", string orderBy = "", string orderType = "",string action ="")
        {
            var data = _customerPackageTransaction.SearchAllFileds(keyword);
            data = GetByCurrentSalon(data);
            data = GetByCurrentSpaBranch(data);
            data = data.Where(e => e.CustomerId == customerId);
            data = data.Where(e => e.Day == day);
            data = data.Where(e => e.Month == month);
            data = data.Where(e => e.Year == year);
            data = data.Where(e => e.PackageId == packageId);
            data = string.IsNullOrEmpty(action)? data: data.Where(e => e.Action.Equals(action));
            data = data.Include(e => e.InvoiceDetail).ThenInclude(e => e.CommissionArrangement).ThenInclude(e => e.SaleStaff);
            data = data.Include(e => e.InvoiceDetail).ThenInclude(e => e.CommissionArrangement).ThenInclude(e => e.ServiceStaff);
            return OkList(data);
        }
        // GET: api/CustomerPackageTransactions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomerPackageTransaction([FromRoute] long id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var customerPackageTransaction = await _customerPackageTransaction.FindAsync(id);

                if (customerPackageTransaction == null)
                {
                    return NotFound();
                }
                return Ok(customerPackageTransaction);
            }
            catch (Exception e)
            {

                  throw new UnexpectedException(id, e);
            }
        }

        // PUT: api/CustomerPackageTransactions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomerPackageTransaction([FromRoute] long id, [FromBody] CustomerPackageTransaction customerPackageTransaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != customerPackageTransaction.Id)
            {
                return BadRequest();
            }
            try
            {
                customerPackageTransaction.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals("emailAddress"));
                await _customerPackageTransaction.EditAsync(customerPackageTransaction);
                return CreatedAtAction("GetCustomerPackageTransaction", new { id = customerPackageTransaction.Id }, customerPackageTransaction);
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerPackageTransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }           
            catch (Exception e)
            {

                  throw new UnexpectedException(customerPackageTransaction,e);
            }
        }

        // POST: api/CustomerPackageTransactions
        [HttpPost]
        public async Task<IActionResult> PostCustomerPackageTransaction([FromBody] CustomerPackageTransaction customerPackageTransaction)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                customerPackageTransaction.CreatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals("emailAddress"));
                await _customerPackageTransaction.AddAsync(customerPackageTransaction);
                return CreatedAtAction("GetCustomerPackageTransaction", new { id = customerPackageTransaction.Id }, customerPackageTransaction);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(customerPackageTransaction,e);
            }
          
        }

        // DELETE: api/CustomerPackageTransactions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomerPackageTransaction([FromRoute] long id)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var customerPackageTransaction = await _customerPackageTransaction.FindAsync(id);
                if (customerPackageTransaction == null)
                {
                    return NotFound();
                }

                await _customerPackageTransaction.DeleteAsync(customerPackageTransaction);

                return Ok(customerPackageTransaction);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(id,e);
            }
          
        }

        private bool CustomerPackageTransactionExists(long id)
        {
            return _customerPackageTransaction.Any<CustomerPackageTransaction>(e => e.Id == id);
        }

        private IQueryable<CustomerPackageTransaction> GetByCurrentSpaBranch(IQueryable<CustomerPackageTransaction> data)
        {
            var currentSalonBranch = _user.Find(JwtHelper.GetIdFromToken(User.Claims)).SalonBranchCurrentId;

            if (currentSalonBranch != default || currentSalonBranch != 0)
            {
                data = data.Where(e => e.SalonBranchId == currentSalonBranch);
            }
            return data;
        }
        private IQueryable<CustomerPackageTransaction> GetByCurrentSalon(IQueryable<CustomerPackageTransaction> data)
        {
            data = data.Where(e => e.SalonId == JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID)));
            return data;
        }    
        private IQueryable<Booking> OrderBy(IQueryable<Booking> data, string orderType)
        {
            if (ORDERTYPE.ASC.Equals(orderType.ToUpper()))
            {
                data = data.OrderBy(e => e.Date.Value);
            }
            else
            {
                data = data.OrderByDescending(e => e.Date.Value);
            }
            return data;
        }
    }
}

