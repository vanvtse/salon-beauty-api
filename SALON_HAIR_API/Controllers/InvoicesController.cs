﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using ULTIL_HELPER;
using Microsoft.AspNetCore.Authorization;
using SALON_HAIR_API.Exceptions;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using SALON_HAIR_API.ViewModels;
using SALON_HAIR_API.Measure;

namespace SALON_HAIR_API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class InvoicesController : CustomControllerBase
    {
        private readonly IInvoice _invoice;
        private readonly IPackage _package;
        private readonly IInvoiceDetail _invoiceDetail;
        private readonly IUser _user;
        private readonly IBooking _booking;
        public InvoicesController(IBooking booking,IPackage package,IInvoiceDetail invoiceDetail,IInvoice invoice, IUser user)
        {
            _booking = booking;
            _package = package;
            _invoiceDetail = invoiceDetail;        
            _invoice = invoice;
            _user = user;
        }
        // GET: api/Invoices
        [HttpGet]
        public IActionResult GetInvoice(bool? isDisplay,int page = 1, int rowPerPage = 50, string keyword = "", string orderBy = "", string orderType = "",string date ="",string paymentStatus="")
        {
            var data = _invoice.SearchAllFileds(keyword);
            data = GetByCurrentSalon(data);
            data = GetByCurrentSpaBranch(data);
            data = data.Where(e => e.Created.Value.Date == GetDateRangeQuery(date).Date);
            data = string.IsNullOrEmpty(paymentStatus) ? data: data.Where(e => e.PaymentStatus.Equals(paymentStatus));
            data = isDisplay.HasValue? data.Where(e => e.IsDisplay== isDisplay) :data;
            data = OrderBy(data, orderType);
            data = data.Include(e=>e.Customer);
            //var dataReturn = _invoice.LoadAllInclude(data,nameof(WarehouseTransaction),nameof(Staff),nameof(Booking),nameof(User));
            return OkList(data);
        }
        [HttpGet("by-customer/{customerId}")]
        public IActionResult GetInvoice(long customerId, int page = 1, int rowPerPage = 50, string keyword = "", string orderBy = "", string orderType = "")
        {
            var data = _invoice.SearchAllFileds(keyword);
            data = GetByCurrentSalon(data);
            data = GetByCurrentSpaBranch(data);
            data = data.Where(e => e.CustomerId == customerId);
            data = OrderBy(data, orderType);
            //var dataReturn = _invoice.LoadAllInclude(data);
            return OkList(data);
        }
        // GET: api/Invoices/5
        [MeasureController("GetInvoice")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInvoice([FromRoute] long id)
        {
            try
            {
                var start = DateTime.Now;
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var invoices =  _invoice.FindBy(e=>e.Id==id);
                invoices = _invoice.LoadAllCollecttion(invoices, nameof(InvoiceStaffArrangement),nameof(WarehouseTransaction));
                invoices = _invoice.LoadAllInclude(invoices);              
                var dataturn = await invoices.FirstOrDefaultAsync();           
                var end = DateTime.Now;
                if (dataturn == null)
                    return NotFound();
                return Ok(dataturn);
            }
            catch (Exception e)
            {
                  throw new UnexpectedException(id, e);
            }
        }     
        // PUT: api/Invoices/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInvoice([FromRoute] long id, [FromBody] Invoice invoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != invoice.Id)
            {
                return BadRequest();
            }
            try
            {
                Invoice invoiceUpdate = _invoice.Find(invoice.Id);
                if (invoiceUpdate == null) throw new Exception("INVOICE_NOT_FOUND");
                //invoice.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                invoiceUpdate.DiscountUnit = invoice.DiscountUnit;
                //invoiceUpdate.IsDisplay = invoice.IsDisplay;
                invoiceUpdate.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                invoiceUpdate.DiscountValue = invoice.DiscountValue;
                invoiceUpdate.DiscountTotal = invoice.DiscountUnit.Equals(DISCOUNTUNIT.MONEY) ? invoice.DiscountValue : invoiceUpdate.TotalDetails * (invoice.DiscountValue / 100);
                invoiceUpdate.Total = invoiceUpdate.TotalDetails - invoiceUpdate.DiscountTotal;
                invoiceUpdate.CustomerId = invoice.CustomerId;
                //invoice.Total = invoice.DiscountUnit.Equals(DISCOUNTUNIT.MONEY) ? invoice.TotalDetails - invoice.DiscountValue : invoice.TotalDetails * (1 - invoice.DiscountValue / 100);
                //invoice.TotalDetails = invoiceUpdate.TotalDetails;
                //invoice.Total = invoice.DiscountUnit.Equals(DISCOUNTUNIT.MONEY) ? invoice.TotalDetails - invoice.DiscountValue : invoice.TotalDetails * (1 - invoice.DiscountValue / 100);
                await _invoice.EditAsync(invoiceUpdate);
                invoice = _invoice.LoadAllCollecttion(invoiceUpdate) ;                
                return CreatedAtAction("GetInvoice", new { id = invoice.Id } , invoiceUpdate);
            }
            catch (Exception e)
            {
                  throw new UnexpectedException(invoice,e);
            }
        }
        [HttpPut("change-customer/{id}")]
        public async Task<IActionResult> PutAsChangeCustomerInvoice([FromRoute] long id, [FromBody] Invoice invoice)
        {
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != invoice.Id)
            {
                return BadRequest();
            }
            try
            {
                Invoice invoiceUpdate = _invoice.Find(invoice.Id);
                invoiceUpdate.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                invoiceUpdate.CustomerId = invoice.CustomerId;
                await _invoice.EditAsync(invoiceUpdate);

                invoiceUpdate = _invoice.LoadAllCollecttion(invoiceUpdate);

                return CreatedAtAction("GetInvoice", new { id = invoice.Id }, invoiceUpdate);
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (Exception e)
            {
                throw new UnexpectedException(invoice, e);
            }
        }
        [HttpPut("close-invoice/{id}")]
        public async Task<IActionResult> PutAsCloseInvoice([FromRoute] long id, [FromBody] Invoice invoice)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != invoice.Id)
            {
                return BadRequest();
            }
            try
            {
                Invoice invoiceUpdate = _invoice.Find(invoice.Id);

                invoiceUpdate.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                invoiceUpdate.IsDisplay = false;
                await _invoice.EditAsync(invoiceUpdate);

                invoiceUpdate = _invoice.LoadAllCollecttion(invoiceUpdate);

                return CreatedAtAction("GetInvoice", new { id = invoice.Id }, invoiceUpdate);
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (Exception e)
            {
                throw new UnexpectedException(invoice, e);
            }
        }
        // POST: api/Invoices
        [HttpPost]
        public async Task<IActionResult> PostInvoice([FromBody] Invoice invoice)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                invoice.CreatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                invoice.SalonId =  long.Parse( JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.SALONID)));
                invoice.CashierId = long.Parse(JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.CURRENT_USER_ID)));
                await _invoice.AddAsync(invoice);
                invoice = _invoice.LoadAllReference(invoice);
                return CreatedAtAction("GetInvoice", new { id = invoice.Id }, invoice);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(invoice,e);
            }
          
        }
        // DELETE: api/Invoices/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInvoice([FromRoute] long id)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var invoice = await _invoice.FindAsync(id);
                if (invoice == null)
                {
                    return NotFound();
                }
                invoice.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                await _invoice.DeleteAsResetAsync(invoice);
                //var booking = 
                //_booking.ResetAsNew()
                return Ok(invoice);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(id,e);
            }
          
        }
        [HttpPut("show-invoice")]
        public async Task<IActionResult> ShowInvoice([FromBody] ListInvoiceIdVM invoiceids)
        {
            //return null;
            try
            {
               var invoces = _invoice.FindBy(e=> invoiceids.Ids.Contains(e.Id));
                await invoces.ForEachAsync(e => e.IsDisplay = true);
                await _invoice.EditRangeAsync(invoces);
                invoces = _invoice.LoadAllInclude(invoces, nameof(WarehouseTransaction));
                //await Task.WhenAll(t1,t2);
                return Ok(invoces);
            }
            catch (Exception e)
            {

                throw new UnexpectedException(invoiceids, e);
            }
        }
        [HttpPut("pay-invoice/{id}")]
        public async Task<IActionResult> PayInvoice([FromRoute] long id, [FromBody] Invoice invoice)
        {
            //return null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != invoice.Id)
            {
                return BadRequest();
            }

            try
            {
                var dataUpdate = _invoice.Find(id);
                if (PAYSTATUS.PAID.Equals(dataUpdate.PaymentStatus))
                {
                    throw new Exception("INVOICE_HAS_BEEN_PAID");
                }
                dataUpdate.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                dataUpdate.IsDisplay = false;
                dataUpdate.NotePayment = invoice.NotePayment;
                dataUpdate.DiscountTotal = dataUpdate.DiscountUnit.Equals(DISCOUNTUNIT.MONEY) ? ((decimal)dataUpdate.DiscountValue)
                    : ((dataUpdate.TotalDetails * (decimal)dataUpdate.DiscountValue / 100));
                //invoice.InvoicePayment.ToList().ForEach(e => e.InvoiceBankingId = e.InvoiceBankingId == default ? 0 : e.InvoiceBankingId);

                var invoicePayment = invoice.InvoicePayment.Where(e=>e.InvoiceMethodId!=default).GroupBy(e => new {e.InvoiceMethodId,e.InvoiceBankingId }).Select(e => new InvoicePayment
                {
                    InvoiceMethodId = e.Key.InvoiceMethodId,
                    InvoiceBankingId = e.Key.InvoiceBankingId,
                    Total = e.Sum(c => c.Total),
                    SalonId = dataUpdate.SalonId,
                    SalonBranchId = dataUpdate.SalonBranchId
                });
                //invoice.InvoicePayment.ToList().ForEach(e => { e.SalonId = dataUpdate.SalonId; e.SalonBranchId = dataUpdate.SalonBranchId; });
                //dataUpdate.Total = invoice.Total;
                dataUpdate.Total = dataUpdate.DiscountUnit.Equals(DISCOUNTUNIT.MONEY) ? (dataUpdate.TotalDetails - (decimal)dataUpdate.DiscountValue) 
                    : (dataUpdate.TotalDetails - (dataUpdate.TotalDetails*(decimal)dataUpdate.DiscountValue/100));
                dataUpdate.InvoicePayment = invoicePayment.ToList();
                dataUpdate.CashBack = invoicePayment.Sum(e => e.Total) - dataUpdate.Total + dataUpdate.Prepay;
                dataUpdate.CashBack = dataUpdate.CashBack > 0 ? dataUpdate.CashBack : 0;
                dataUpdate.PaymentStatus = PAYSTATUS.PAID;
                await _invoice.EditAsPayAsync(dataUpdate);                           
                dataUpdate = _invoice.LoadAllReference(dataUpdate);               
                return CreatedAtAction("GetInvoice", new { id = invoice.Id }, dataUpdate);
            }
            catch (Exception e)
            {
                throw new UnexpectedException(invoice, e);
            }
        }
        [HttpPut("cancel-invoice/{id}")]
        public async Task<IActionResult> CancelInvoice([FromRoute] long id, [FromBody] Invoice invoice)
        {
            //return null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != invoice.Id)
            {
                return BadRequest();
            }

            try
            {
                var dataUpdate = _invoice.Find(id);
                if (OBJECTSTATUS.CANCEL.Equals(dataUpdate.Status)|| (!PAYSTATUS.PAID.Equals(dataUpdate.PaymentStatus)))
                {
                    throw new Exception("CAN_NOT_CANCEL");
                }
                dataUpdate.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                dataUpdate.Updated = DateTime.Now;
                dataUpdate.PaymentStatus = PAYSTATUS.UNPAID;
                await _invoice.EditAsCancelAsync(dataUpdate,OBJECTSTATUS.DELETED);
                dataUpdate = _invoice.LoadAllReference(dataUpdate);
                return CreatedAtAction("GetInvoice", new { id = invoice.Id }, dataUpdate);
            }
            catch (Exception e)
            {
                throw new UnexpectedException(invoice, e);
            }

        }
        [HttpPut("delete-paid-invoice/{id}")]
        public async Task<IActionResult> DeletePaidInvoice([FromRoute] long id, [FromBody] Invoice invoice)
        {
            //return null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != invoice.Id)
            {
                return BadRequest();
            }

            try
            {
                var dataUpdate = _invoice.Find(id);
                if (OBJECTSTATUS.DELETED.Equals(dataUpdate.Status)|| (!PAYSTATUS.PAID.Equals(dataUpdate.PaymentStatus)))
                {
                    throw new Exception("CAN_NOT_DELETED");
                }
                dataUpdate.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                dataUpdate.Updated = DateTime.Now;
                dataUpdate.Status = OBJECTSTATUS.DELETED;
                dataUpdate.PaymentStatus = PAYSTATUS.CANCEL;
                await _invoice.EditAsCancelAsync(dataUpdate,OBJECTSTATUS.DELETED);
                dataUpdate = _invoice.LoadAllReference(dataUpdate);
                return CreatedAtAction("GetInvoice", new { id = invoice.Id }, dataUpdate);
            }
            catch (Exception e)
            {
                throw new UnexpectedException(invoice, e);
            }

        }
        [HttpPut("use-package/{id}")]
        public async Task<IActionResult> UsePackageInvoice([FromRoute] long id, [FromBody] Invoice invoice)
        {
            //return null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != invoice.Id)
            {
                return BadRequest();
            }

            try
            {
                var dataUpdate = _invoice.Find(id);
                if (PAYSTATUS.PAID.Equals(dataUpdate.PaymentStatus))
                {
                    throw new Exception("INVOICE_HAS_BEEN_PAID");
                }
                dataUpdate.UpdatedBy = JwtHelper.GetCurrentInformation(User, e => e.Type.Equals(CLAIMUSER.EMAILADDRESS));
                dataUpdate.IsDisplay = false;
                dataUpdate.NotePayment = invoice.NotePayment;
                dataUpdate.PaymentStatus = PAYSTATUS.PAID;
                 await _invoice.EditAsUsePackageAsync(dataUpdate);
                dataUpdate = _invoice.LoadAllReference(dataUpdate);
                return CreatedAtAction("GetInvoice", new { id = invoice.Id }, dataUpdate);
            }
            catch (Exception e)
            {
                throw new UnexpectedException(invoice, e);
            }

        }
        private bool InvoiceExists(long id)
        {
            return _invoice.Any<Invoice>(e => e.Id == id);
        }       
        private IQueryable<Invoice> GetByCurrentSpaBranch(IQueryable<Invoice> data)
        {
            var currentSalonBranch = _user.Find(JwtHelper.GetIdFromToken(User.Claims)).SalonBranchCurrentId;

            if (currentSalonBranch != default || currentSalonBranch != 0)
            {
                data = data.Where(e => e.SalonBranchId == currentSalonBranch);
            }
            return data;
        }
        private IQueryable<Invoice> GetByCurrentSalon(IQueryable<Invoice> data)
        {
            var salonId = JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));
            data = data.Where(e => e.SalonId == salonId);
            return data;
        }
        private DateTime GetDateRangeQuery(string date)
        {
            date += "";

            var st = DateTime.Now;
            if (!string.IsNullOrEmpty(date))
            {
                st = DateTime.Parse(date);
            }

            return st;
        }
        private IQueryable<Invoice> OrderBy(IQueryable<Invoice> data, string orderType)
        {
            if (orderType.ToUpper().Equals(ORDERTYPE.ASC))
            {
                data = data.OrderBy(e => e.Id);
            }
            else
            {
                data = data.OrderByDescending(e => e.Id);
            }
            return data;
        }
    }
}
