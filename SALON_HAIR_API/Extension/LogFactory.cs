﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ULTIL_HELPER;

namespace SALON_HAIR_API.Extension
{
    public static class LogFactory
    {
        public static void Log(string logMessage, string type = "info")
        {
            try
            {
                string strLogMessage = string.Empty;
                String fName = DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
                StreamWriter swLog;

                strLogMessage = string.Format("{0}: {1}", DateTime.Now, logMessage);
                var current = Directory.GetCurrentDirectory();
                var originalDirectory = new DirectoryInfo($"wwwroot\\{current}\\Logs\\{type}\\");
                string pathString = Path.Combine(originalDirectory.ToString(), DateTime.Now.ToString("yyyy-MM"));
                //var fileName1 = Path.GetFileName(fName);
                //create directory if does not exist
                bool isExists = Directory.Exists(pathString);
                if (!isExists) Directory.CreateDirectory(pathString);
                //generate local path to save file
                var path = $"{pathString}\\{fName}";
                if (!File.Exists(path))
                {

                    FileStream fs = new FileStream(path, FileMode.CreateNew);
                    StreamWriter swr = new StreamWriter(fs);
                    try
                    {
                        swr.Close();
                        fs.Close();
                    }
                    catch (Exception )
                    {
                        swr.Close();
                        fs.Close();
                    }
                }

                if (!File.Exists(path))
                {
                    swLog = new StreamWriter(path);
                }
                else
                {
                    swLog = File.AppendText(path);
                }

                swLog.WriteLine(strLogMessage);
                swLog.WriteLine();
                swLog.Close();
            }
            catch (Exception)
            {

            }
        }
        public static void Log(object logMessage, string type = "info")
        {
            Log(StringHelper.SerializeCamelCase(logMessage), type);
        }
    }
}
