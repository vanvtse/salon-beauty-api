﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SALON_HAIR_CORE.Interface;
using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_IMPORT.Model;
using ULTIL_HELPER;

namespace SALON_HAIR_API.ImportControllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]    
    public class ImportController : CustomControllerBase
    {
        private IImportHelper _importHelper;
        private ICustomer _customer;
        public ImportController(ICustomer customer,IImportHelper importHelper)
        {
            _customer = customer;
            _importHelper = importHelper;
        }
        [HttpPost("customer")]
        public IActionResult ImportInformation(IFormCollection file)
        {
            var fileImage = file.Files[0];
            if (fileImage == null || fileImage.Length == 0)
                return Content("file not selected");
            var excelStream = fileImage.OpenReadStream();
            var data = _importHelper.ReadExcelFile(excelStream);
            var list = _importHelper.DataTableToList<Customer>(data);
            _customer.AddAsImport(list);
            return Ok(list);
        }
    }
}