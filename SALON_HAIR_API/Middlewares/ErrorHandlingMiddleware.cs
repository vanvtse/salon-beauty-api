﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using SALON_HAIR_API.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using SALON_HAIR_ENTITY.Entities;
using System.Threading.Tasks;
using ULTIL_HELPER;
using System.Text;

namespace SALON_HAIR_API.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private ILogHelper _logHelper;
        private salon_hairContext _dbContext;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;

        }
        public async Task Invoke(HttpContext context, ILogHelper logHelper, salon_hairContext dbContext /* other scoped dependencies */)
        {
            try
            {
                _dbContext = dbContext;
                _logHelper = logHelper;
                await _next(context);
            }
            catch (UnexpectedException unexpectedException)
            {
                await HandleExceptionDetailAsync(context, unexpectedException);
            }
            catch (BadRequestException ex)
            {
                await HandleExceptionDetailAsync(context, ex);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
            finally
            {

            }

        }
        private async Task HandleExceptionDetailAsync(HttpContext context, BadRequestException exception)
        {
            string message = exception.InnerException == null ? exception.Message : exception.InnerException.Message;
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exception.HttpCode;
            var respone = context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                //error = message,
                errorCode = exception.HttpCode,
                errorDesc = message,
            }));
            try
            {
                await LogAsync(context, exception);
            }
            catch (Exception e)
            {

            }
            await respone;
        }
        private async Task HandleExceptionDetailAsync(HttpContext context, UnexpectedException unexpectedException)
        {           
            string message = GetMessageException(unexpectedException);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = unexpectedException.HttpCode;
            var respone = context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                errorCode = unexpectedException.HttpCode,
                errorDesc = message,
            }));
            try
            {
                var exception = unexpectedException.exception == null ? unexpectedException : unexpectedException.exception;
                var _data = new
                {
                    _Soure = exception.InnerException?.Source ?? exception.Source,
                    //_Soure = exception.InnerException == null ? exception.Source : exception.InnerException.Source,
                    _StackTrace = exception.InnerException == null ? exception.StackTrace : exception.InnerException.StackTrace,
                    _Data = unexpectedException.DataLog,
                    _Message = message,
                };
                await LogAsync(context, _data, _data._Soure, _data._StackTrace, _data._Data, _data._Message);
                //await LogAsync(context, unexpectedException);
            }
            catch (Exception)
            {

            }
            await respone;
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            string message = "";
            if (exception.InnerException != null)
            {
                message = exception.InnerException.Message;
            }
            else
            {
                message = exception.Message;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = 500;
            var respone = context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                //error = message,
                errorCode = 500,
                errorDesc = message,
            }));
            try
            {
                await LogAsync(context, exception);
            }
            catch (Exception)
            {

            }
            await respone;
        }
        private async Task LogAsync(HttpContext context, Exception exception)
        {
            var _data = new
            {
                _Soure = exception.InnerException == null ? exception.Source : exception.InnerException.Source,
                _StackTrace = exception.InnerException == null ? exception.StackTrace : exception.InnerException.StackTrace,
                _Data = exception.InnerException == null ? exception.Data : exception.InnerException.Data,
                _Message = exception.InnerException == null ? exception.Message : exception.InnerException.Message,

            };
            await LogAsync(context, _data);
        }
        private async Task LogAsync(HttpContext context, UnexpectedException unexpectedException)
        {
            var exception = unexpectedException.exception == null ? unexpectedException : unexpectedException.exception;
            var _data = new
            {
                _Soure = exception.InnerException?.Source ?? exception.Source,
                //_Soure = exception.InnerException == null ? exception.Source : exception.InnerException.Source,
                _StackTrace = exception.InnerException == null ? exception.StackTrace : exception.InnerException.StackTrace,
                _Data = unexpectedException.DataLog,
                _Message = exception.InnerException == null ? exception.Message : exception.InnerException.Message,
            };
            await LogAsync(context, _data,_data._Soure,_data._StackTrace,_data._Data,_data._Message);
        }
        private async Task LogAsync(HttpContext context, object data, string soure, string stackTrace, object dataLog, string message)
        {
            await LogAsync(context, data);
            await WriteExceptionLogToBD(context.TraceIdentifier,
              JsonConvert.SerializeObject(dataLog),
              context.Request.Method, context.Request.Path.Value,
              context.Request.Headers.Where(e => e.Key.Equals("Authorization")).Select(e => e.Value).FirstOrDefault(),
              message,
              soure,
              stackTrace
              );
        }
        private async Task LogAsync(HttpContext context, BadRequestException exception)
        {
            var _data = new
            {
                _Soure = exception.InnerException == null ? exception.Source : exception.InnerException.Source,
                _StackTrace = exception.InnerException == null ? exception.StackTrace : exception.InnerException.StackTrace,
                _Data = exception.DataLog,
                _Message = exception.InnerException == null ? exception.Message : exception.InnerException.Message,

            };
            await LogAsync(context, _data);
        }
        private async Task LogAsync(HttpContext context, object data)
        {
            object datalog = new
            {
                context.TraceIdentifier,
                _method = context.Request.Method,
                _url = context.Request.Path.Value,
                _tooken = context.Request.Headers.Where(e => e.Key.Equals("Authorization")).Select(e => e.Value).FirstOrDefault(),
                _data = data

            };
            //  , "wwwroot", "exception");

            var setting = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            string json = "";
            try
            {
                json = JsonConvert.SerializeObject(datalog);

            }
            catch (Exception)
            {
                try
                {
                    json = JsonConvert.SerializeObject(
                     new
                     {
                         context.TraceIdentifier,
                         _method = context.Request.Method,
                         _url = context.Request.Path.Value,
                         _tooken = context.Request.Headers.Where(e => e.Key.Equals("Authorization")).Select(e => e.Value).FirstOrDefault(),

                     }
             );
                }
                catch (Exception cc)
                {
                    await _logHelper.LogAsync(new { message = cc.Message, cc.StackTrace }, "wwwroot", "exception");
                }

            }
          
            await _logHelper.LogAsync(json, "wwwroot", "exception");
        }
        private async Task WriteExceptionLogToBD(string traceIdentifier, string data, string method, string path, string tooken, string message, string source, string stackTrace)
        {
            try
            {
                var logException = new LogException
                {
                    TraceIdentifier = traceIdentifier,
                    Data = UTF8Encoding.UTF8.GetString(UTF8Encoding.UTF8.GetBytes(data)),
                    Message = message,
                    Method = method,
                    Path = path,
                    Source = source,
                    StackTrace = stackTrace,
                    Tooken = tooken
                };
             
                //await _dbContext.SaveChangesAsync();
                await _dbContext.Database.ExecuteSqlCommandAsync("CALL `salon_hair`.`sp_create_log_exception`(@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);",
                   new MySqlParameter("@p1", logException.TraceIdentifier),
                   new MySqlParameter("@p2", logException.Data),
                   new MySqlParameter("@p3", logException.Method),
                   new MySqlParameter("@p4", logException.Path),
                   new MySqlParameter("@p5", logException.Tooken),
                   new MySqlParameter("@p6", logException.Message),
                   new MySqlParameter("@p7", logException.Source ),
                   new MySqlParameter("@p8", logException.StackTrace)
                   );
            }
            catch (Exception e)
            {
                throw;
            }

            
        }
        private string GetMessageException(UnexpectedException exception)
        {

            if (exception.exception.InnerException is MySqlException)
            {
                var ex = exception.exception.InnerException as MySqlException;
                var sourceError = MysqlExcetpionMessage.indexRefernces.Where(e => ex.Message.Contains(e.Index)).FirstOrDefault() ?.Description;
                var rootError = MysqlExcetpionMessage.messageReferences.Where(e => e.Number == ex.Number && e.SqlState.Equals(ex.SqlState)).FirstOrDefault()?.Symbol ?? ex.Message;
                return sourceError +( string.IsNullOrEmpty(sourceError)?"": "_")+ rootError;
            }
            return exception.exception.InnerException == null ? exception.exception.Message : exception.exception.InnerException.Message;
        }
    }
    //Extension method used to add the middleware to the HTTP request pipeline.
    public static class ErrorHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }
}
