﻿using SALON_HAIR_CORE.Interface;
using ULTIL_HELPER;
using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.AspNetCore.Authorization;
using SALON_HAIR_ENTITY.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.IO;
using SALON_HAIR_REPORT.Interface;
using SALON_HAIR_REPORT.Model;

namespace SALON_HAIR_API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class ReportController : CustomControllerBase
    {
        private IRevenue _revenue;
        private IStaffReport _staffReport;
        private IConfiguration _configuration;
        private IInvoicePayment _invoicePayment;
        private IPaymentMethod _paymentMethod;
        private IInvoice _invoice;
        private ICommissionArrangement _commissionArrangement;
        private IExportHelper _exportHelper;
        private IInvoiceDetail _invoiceDetail;
        public ReportController(IStaffReport staffReport,IRevenue revenue,IInvoiceDetail invoiceDetail, IConfiguration configuration, IExportHelper exportHelper, ICommissionArrangement commissionArrangement, IInvoice invoice, IInvoicePayment invoicePayment, IPaymentMethod paymentMethod)
        {
            _staffReport = staffReport;
            _revenue = revenue;
            _invoiceDetail = invoiceDetail;
            _configuration = configuration;
            _exportHelper = exportHelper;
            _commissionArrangement = commissionArrangement;
            _paymentMethod = paymentMethod;
            _invoicePayment = invoicePayment;
            _invoice = invoice;
        }
        [HttpGet("commision-staff")]
        public IActionResult CommissionStaff(string salonBranchIds = "", string start = "", string end = "")
        {
             var salonId = GetCurrentSalonId();
            return OkList(_staffReport.GetCommissionStaff(salonId,salonBranchIds, start, end));
        }
        [HttpGet("commision-staff-export")]
        public IActionResult CommissionStaffExport(string salonBranchIds = "", string start = "", string end = "")
        {
            var salonId = GetCurrentSalonId();
            string url = _exportHelper.CreateExcelFile("CommissionStaffExport", _staffReport.GetCommissionStaff(salonId, salonBranchIds, start, end).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("commision-staff-detail")]
        public IActionResult CommissionStaffDetail(long staffId, string start = "", string end = "")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_staffReport.GetCommissionStaffDetail(salonId, staffId, start, end));
        }
        [HttpGet("commision-staff-detail-export")]
        public IActionResult CommissionStaffDetailExport(long staffId, string start = "", string end = "")
        {         
            var salonId = GetCurrentSalonId();
            string url = _exportHelper.CreateExcelFile("CommissionStaffDetailExport", _staffReport.GetCommissionStaffDetail(salonId, staffId, start, end).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });            
        }
        [HttpGet("revenue-payment-method-general")]
        public IActionResult RevenuePaymentMethodGeneral(string salonBranchIds = "", string paymentMethodCodes = "", string start = "", string end = "")
        {
            var salonId = GetCurrentSalonId();
            var rs = _revenue.GetRevenuePaymentMethodGeneralVM(salonId, salonBranchIds, paymentMethodCodes, start, end);
            return OkList(rs);
        }
        [HttpGet("revenue-payment-method-detail")]
        public IActionResult RevenuePaymentMethodDetail(string salonBranchIds = "", string paymentMethodCodes = "", string start = "", string end = "")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_revenue.GetRevenuePaymentMethodDetailVM(salonId,salonBranchIds, paymentMethodCodes, start, end));
        }
        [HttpGet("revenue-payment-method-detail-export")]
        public IActionResult RevenuePaymentMethodGeneralExport(string salonBranchIds = "", string paymentMethodCodes = "", string start = "", string end = "")
        {
            var salonId = GetCurrentSalonId();
            string url = _exportHelper.CreateExcelFile("RevenuePaymentMethodGeneralExport", _revenue.GetRevenuePaymentMethodDetailVM(salonId,salonBranchIds, paymentMethodCodes, start, end).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("revenue-service-detail")]
        public IActionResult RevenueServiceDetail(string salonBranchIds = "", string serviceIds = "", string start = "", string end = "", string keyword = "")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_revenue.GetRevenueServiceDetail(salonId,salonBranchIds, serviceIds,start,end,keyword));
        }
        [HttpGet("revenue-service-detail-export")]
        public IActionResult RevenueServiceDetailExport(string salonBranchIds = "", string serviceIds = "", string start = "", string end = "", string keyword = "")
        {
            var salonId = GetCurrentSalonId();
            string url = _exportHelper.CreateExcelFile("RevenuePaymentMethodGeneralExport", _revenue.GetRevenueServiceDetail(salonId,salonBranchIds, serviceIds, start, end, keyword).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("revenue-product-detail")]
        public IActionResult RevenueProductDetail(string salonBranchIds = "", string productIds = "", string start = "", string end = "", string keyword = "")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_revenue.GetRevenueProductDetail(salonId, salonBranchIds, productIds, start, end, keyword));
        }
        [HttpGet("revenue-product-detail-export")]
        public IActionResult RevenueProductDetailExport(string salonBranchIds = "", string productIds = "", string start = "", string end = "", string keyword = "")
        {
            var salonId = GetCurrentSalonId();
            string url = _exportHelper.CreateExcelFile("RevenuePaymentMethodGeneralExport", _revenue.GetRevenueProductDetail(salonId, salonBranchIds, productIds, start, end, keyword).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("revenue-invoice-detail")]
        public IActionResult RevenueInvoiceDetail(string salonBranchIds = "", string start = "", string end = "", string keyword = "")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_revenue.GetRevenueInvoiceDetail(salonId, salonBranchIds, start, end, keyword));
        }
        [HttpGet("revenue-invoice-detail-export")]
        public IActionResult RevenueInvoiceDetailExport(string salonBranchIds = "", string start = "", string end = "", string keyword = "")
        {          
            var salonId = GetCurrentSalonId();
            string url = _exportHelper.CreateExcelFile("RevenueInvoiceDetail", _revenue.GetRevenueInvoiceDetail(salonId, salonBranchIds, start, end, keyword).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("revenue-date-detail")]
        public IActionResult RevenueDateDetail(string salonBranchIds = "", string start = "", string end = "", string keyword = "",string type="")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_revenue.GetRevenueDateDetail(salonId, salonBranchIds, start, end, keyword, type));
        }
        [HttpGet("revenue-date-detail-export")]
        public IActionResult RevenueDateDetailExport(string salonBranchIds = "", string start = "", string end = "", string keyword = "", string type = "")
        {
            var salonId = GetCurrentSalonId();         
            string url = _exportHelper.CreateExcelFile("RevenueDateDetailExport", _revenue.GetRevenueDateDetail(salonId, salonBranchIds, start, end, keyword, type).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("customer-frequency")]
        public IActionResult CustomerFrequency(string salonBranchIds = "", string start = "", string end = "", string keyword = "", string type = "")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_revenue.GetCustomerFrequency(salonId, salonBranchIds, start, end, keyword, type));
        }
        [HttpGet("customer-frequency-export")]
        public IActionResult CustomerFrequencyExport(string salonBranchIds = "", string start = "", string end = "", string keyword = "", string type = "")
        {
            var salonId = GetCurrentSalonId();         
            string url = _exportHelper.CreateExcelFile("CustomerFrequency", _revenue.GetCustomerFrequency(salonId, salonBranchIds, start, end, keyword, type).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("revenue-expenditure")]
        public IActionResult RevenueExpenditure(string salonBranchIds = "", string start = "", string end = "", string keyword = "", string action = "",string cash_book_transaction_category_id="")
        {
            var salonId = GetCurrentSalonId();
            return OkList(_revenue.GetRevenueExpenditure(salonId, salonBranchIds, start, end, keyword, action, cash_book_transaction_category_id));
        }
        [HttpGet("revenue-expenditure-export")]
        public IActionResult RevenueExpenditureExport(string salonBranchIds = "", string start = "", string end = "", string keyword = "", string action = "", string cash_book_transaction_category_id = "")
        {            
            var salonId = GetCurrentSalonId();
            string url = _exportHelper.CreateExcelFile("RevenueExpenditureExport", _revenue.GetRevenueExpenditure(salonId, salonBranchIds, start, end, keyword, action, cash_book_transaction_category_id).ToList());
            return Ok(new { Url = Path.Combine(_configuration["MediaURL:value"], url) });
        }
        [HttpGet("total-revenue-expenditure")]
        public IActionResult TotalRevenueExpenditure(string salonBranchIds = "", string start = "", string end = "", string keyword = "", string action = "", string cash_book_transaction_category_id = "")
        {
            var salonId = GetCurrentSalonId();
            var data = _revenue.GetRevenueExpenditure(salonId, salonBranchIds, start, end, keyword, action, cash_book_transaction_category_id);



            var dataReturn = new TotalRevenueExpenditureVM
            {
                TotalExpenditure = data.Where(a => a.Action.Equals(CASHBOOKTRANSACTIONACTION.OUTCOME)).Sum(a => a.Total),
                TotalRevenue = data.Where(a => a.Action.Equals(CASHBOOKTRANSACTIONACTION.INCOME)).Sum(a => a.Total)
            };
            return Ok(dataReturn);
        }
        private long GetCurrentSalonId()
        {
            var salonId = JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));
            return salonId;

        }
    }
}
