﻿using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using ULTIL_HELPER;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace SALON_HAIR_API.Controllers
{
    [Route("Report/[controller]")]
    [ApiController]
    [Authorize]
    public class RevenvueController : CustomControllerBase
    {
        private IInvoicePayment _invoicePayment;
        private IPaymentMethod _paymentMethod;
        public RevenvueController(IInvoicePayment invoicePayment, IPaymentMethod paymentMethod)
        {
            _paymentMethod = paymentMethod;
            _invoicePayment = invoicePayment;
        }

        private IQueryable<InvoicePayment> GetByCurrentSalon(IQueryable<InvoicePayment> data)
        {
            var salonId = JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));
            data = data.Where(e => e.SalonId == salonId);
            return data;
        }
        private Tuple<DateTime, DateTime> GetDateRangeQuery(string start, string end)
        {
            start += "";
            end += "";
            var st = DateTime.Now.AddDays(-30.0);
            var en = DateTime.Now;
            if (!string.IsNullOrEmpty(start))
            {
                st = DateTime.Parse(start);
            }
            if (!string.IsNullOrEmpty(end))
            {
                en = DateTime.Parse(end);
            }
            return Tuple.Create(st, en);
        }
        private List<DateTime> GetDatesBetween(DateTime startDate, DateTime endDate)
        {
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                allDates.Add(date);
            return allDates;

        }
        private IEnumerable<InvoicePayment> GenInvoicePayments(List<DateTime> dateTimes)
        {
            var salonId = JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));
            //var salonId = 1;//JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));
            var paymentMethods = _paymentMethod.GetAll().Where(e => e.SalonId == salonId).ToList();
            var list = new List<InvoicePayment>();
            dateTimes.ForEach(e => {
                paymentMethods.ForEach(c => {
                    list.Add(new InvoicePayment { Created = e, Total = 0, InvoiceMethod = new PaymentMethod { Code = c.Code } });
                });
                
            });
            return list;
        }
    }
}