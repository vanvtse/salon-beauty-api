﻿using SALON_HAIR_CORE.Repository;
using SALON_HAIR_ENTITY.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SALON_HAIR_CORE.Interface
{
    public interface IBookingChannel: IGenericRepository<BookingChannel>
    {

    }
}
