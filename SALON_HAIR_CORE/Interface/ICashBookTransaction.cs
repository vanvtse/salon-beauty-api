
using SALON_HAIR_CORE.Repository;
using SALON_HAIR_ENTITY.Entities;
using System.Linq;

namespace SALON_HAIR_CORE.Interface
{
    public interface ICashBookTransaction : IGenericRepository<CashBookTransaction>
    {
        IQueryable<CashBookTransaction> QueryForGetReport(string salonBranchIds, string start, string end, string keyword, long salonId);
    }
}

