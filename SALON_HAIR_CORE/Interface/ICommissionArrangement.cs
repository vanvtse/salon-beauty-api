
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SALON_HAIR_CORE.Repository;
using SALON_HAIR_ENTITY.Entities;

namespace SALON_HAIR_CORE.Interface
{
    public interface ICommissionArrangement : IGenericRepository<CommissionArrangement>
    {      
        Task EditRangeComputeCommisionAsync(ICollection<CommissionArrangement> invoiceStaffArrangements);
        IQueryable<CommissionArrangement> QueryForGetReport(string salonBranchIds, string start, string end, string keyword, long salonId);
    }
}

