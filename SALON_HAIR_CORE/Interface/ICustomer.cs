
using System.Collections.Generic;
using SALON_HAIR_CORE.Repository;
using SALON_HAIR_ENTITY.Entities;

namespace SALON_HAIR_CORE.Interface
{
    public interface ICustomer : IGenericRepository<Customer>
    {
        void AddAsImport(List<Customer> list);
    }
}

