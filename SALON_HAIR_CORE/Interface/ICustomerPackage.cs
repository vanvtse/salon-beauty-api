
using System.Threading.Tasks;
using SALON_HAIR_CORE.Repository;
using SALON_HAIR_ENTITY.Entities;

namespace SALON_HAIR_CORE.Interface
{
    public interface ICustomerPackage : IGenericRepository<CustomerPackage>
    {
         Task EditAsGenTransactionAsync(CustomerPackage customerPackage,long? salon_branch_id);
    }
}

