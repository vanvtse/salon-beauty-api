
using System.Linq;
using System.Threading.Tasks;
using SALON_HAIR_CORE.Repository;
using SALON_HAIR_ENTITY.Entities;

namespace SALON_HAIR_CORE.Interface
{
    public interface IInvoice : IGenericRepository<Invoice>
    {
        Task EditAsPayAsync(Invoice dataUpdate);      
        Task EditAsCancelAsync(Invoice dataUpdate,string objectStatus);
        IQueryable<Invoice> QueryForGetReport(string salonBranchIds, string start, string end, string keyword, long salonId);
        Task DeleteAsResetAsync(Invoice invoice);
        Task EditAsUsePackageAsync(Invoice dataUpdate);
    }
}

