
using System.Threading.Tasks;
using SALON_HAIR_CORE.Repository;
using SALON_HAIR_ENTITY.Entities;

namespace SALON_HAIR_CORE.Interface
{
    public interface IPackage : IGenericRepository<Package>
    {
        Task<int> EditMany2ManyAsync(Package package);
    }
}

