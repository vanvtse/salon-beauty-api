﻿

using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using SALON_HAIR_CORE.Repository;
using System;
using System.Threading.Tasks;

namespace SALON_HAIR_CORE.Service
{
    public class BookingChannelService : GenericRepository<BookingChannel>, IBookingChannel
    {
        private salon_hairContext _salon_hairContext;
        public BookingChannelService(salon_hairContext salon_hairContext) : base(salon_hairContext)
        {
            _salon_hairContext = salon_hairContext;
        }
        public new void Edit(BookingChannel BookingChannel)
        {
            BookingChannel.Updated = DateTime.Now;

            base.Edit(BookingChannel);
        }
        public async new Task<int> EditAsync(BookingChannel BookingChannel)
        {
            BookingChannel.Updated = DateTime.Now;
            return await base.EditAsync(BookingChannel);
        }
        public new async Task<int> AddAsync(BookingChannel BookingChannel)
        {
            BookingChannel.Created = DateTime.Now;
            return await base.AddAsync(BookingChannel);
        }
        public new void Add(BookingChannel BookingChannel)
        {
            BookingChannel.Created = DateTime.Now;
            base.Add(BookingChannel);
        }
        public new void Delete(BookingChannel BookingChannel)
        {
            BookingChannel.Status = "DELETED";
            base.Edit(BookingChannel);
        }
        public new async Task<int> DeleteAsync(BookingChannel BookingChannel)
        {
            BookingChannel.Status = "DELETED";
            return await base.EditAsync(BookingChannel);
        }
    }
}
