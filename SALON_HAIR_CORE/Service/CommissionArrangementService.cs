

using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using SALON_HAIR_CORE.Repository;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using ULTIL_HELPER;

namespace SALON_HAIR_CORE.Service
{
    public class CommissionArrangementService: GenericRepository<CommissionArrangement> ,ICommissionArrangement
    {
        private salon_hairContext _salon_hairContext;
        public CommissionArrangementService(salon_hairContext salon_hairContext) : base(salon_hairContext)
        {
            _salon_hairContext = salon_hairContext;
        }        
        public new void Edit(CommissionArrangement commissionArrangement)
        {
            commissionArrangement.Updated = DateTime.Now;

            base.Edit(commissionArrangement);
        }
        public async new Task<int> EditAsync(CommissionArrangement commissionArrangement)
        {            
            commissionArrangement.Updated = DateTime.Now;         
            return await base.EditAsync(commissionArrangement);
        }
        public new async Task<int> AddAsync(CommissionArrangement commissionArrangement)
        {
            commissionArrangement.Created = DateTime.Now;
            return await base.AddAsync(commissionArrangement);
        }
        public new void Add(CommissionArrangement commissionArrangement)
        {
            commissionArrangement.Created = DateTime.Now;
            base.Add(commissionArrangement);
        }
        public new void Delete(CommissionArrangement commissionArrangement)
        {
            commissionArrangement.Status = "DELETED";
            base.Edit(commissionArrangement);
        }
        public new async Task<int> DeleteAsync(CommissionArrangement commissionArrangement)
        {
            commissionArrangement.Status = "DELETED";
            return await base.EditAsync(commissionArrangement);
        }

        public Task EditRangeComputeCommisionAsync(ICollection<CommissionArrangement> commissionArrangements)
        {
           
            commissionArrangements = commissionArrangements.Where(e => !(e.ServiceStaffId == default && e.SaleStaffId == default)).ToList();
            #region Get StaffId
            var staffIdsSaleProduct = commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.PRODUCT)).Select(e => e.SaleStaffId);
            var staffIdsSalePackage = commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.PACKAGE)).Select(e => e.SaleStaffId);
            var staffIdsSaleService = commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.SERVICE)).Where(e=>!e.IsPaid.Value).Select(e => e.SaleStaffId);
            var staffIdsDoService = commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.SERVICE)).Select(e => e.ServiceStaffId);
            #endregion
            if (commissionArrangements.Count() == 0) return _salon_hairContext.SaveChangesAsync();
            var salonId = commissionArrangements.FirstOrDefault()?.SalonId;
            var salonBranchId = commissionArrangements.FirstOrDefault()?.SalonBranchId;
            #region Get ProductId,ServiceId,PackageId will be selected
            var productIds = commissionArrangements.ToList()
                .Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.PRODUCT))               
                .Select(e => e.ObjectId);
            var packageIds = commissionArrangements.ToList()
                .Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.PACKAGE))
                .Where(e=>!e.IsPaid.Value)
                .Select(e => e.ObjectId);               
            var serviceIdsSale = commissionArrangements.ToList()
                .Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.SERVICE))      
                .Where(e=>!e.IsPaid.Value)
                .Select(e => e.ObjectId);
            var serviceIdsDo = commissionArrangements.ToList()
                .Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.SERVICE))                
                .Select(e => e.ObjectId);
            #endregion

            #region Get Commsion
            var commisionProduct = _salon_hairContext.CommissionProduct
                .Where(e=>e.SalonBranchId==salonBranchId)
                .Where(e => productIds.Contains(e.ProductId))
                .Where(e=> staffIdsSaleProduct.Contains(e.StaffId))
                .ToList();
            var commisionPackage = _salon_hairContext.CommissionPackage
                .Where(e => e.SalonBranchId == salonBranchId)
                .Where(e => packageIds.Contains(e.PackageId))
                .Where(e => staffIdsSalePackage.Contains(e.StaffId))
                .ToList();
            var commisionServiceSale = _salon_hairContext.CommissionService
                .Where(e => e.SalonBranchId == salonBranchId)
                .Where(e => serviceIdsSale.Contains(e.ServiceId))
                .Where(e => staffIdsSaleService.Contains(e.StaffId))
                .ToList();
            var commisionServiceDo = _salon_hairContext.CommissionService
                .Where(e => e.SalonBranchId == salonBranchId)
                .Where(e => serviceIdsDo.Contains(e.ServiceId))
                .Where(e => staffIdsDoService.Contains(e.StaffId))
                .ToList();
            #endregion

            commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.PRODUCT)).ToList().ForEach(e => {
                //checkeck setting addvance TIMECOMMISION
                var product = commisionProduct
                .Where(c => c.ProductId == e.ObjectId)
                .Where(c => c.SalonBranchId == salonBranchId)
                .Where(c => c.ProductId == e.ObjectId)
                .Where(c=>c.StaffId==e.SaleStaffId)
                .FirstOrDefault();
                if (product != default)
                {
                    e.CommsionValue = GetCommisionValue(product.CommissionValue, product.CommissionUnit, e);
                    e.CommisionValueAffterDiscount = GetCommisionValueAffterDiscount(product.CommissionValue, product.CommissionUnit, e);
                }
            });
            commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.PACKAGE)).Where(e=>!e.IsPaid.Value).ToList().ForEach(e => {
                //checkeck setting addvance TIMECOMMISION
                var product = commisionPackage
                .Where(c => c.PackageId == e.ObjectId)
                .Where(c => c.SalonBranchId == salonBranchId)
                .Where(c => c.StaffId == e.SaleStaffId)
                .FirstOrDefault();
                if (product != default)
                {
                    e.CommsionValue = GetCommisionValue(product.CommissionValue, product.CommissionUnit, e);
                    e.CommisionValueAffterDiscount = GetCommisionValueAffterDiscount(product.CommissionValue, product.CommissionUnit, e);
                }
            });          
            commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.SERVICE)).Where(e => !e.IsPaid.Value).ToList().ForEach(e => {
                //checkeck setting addvance TIMECOMMISION
                var service = commisionServiceSale
                .Where(c => c.ServiceId == e.ObjectId)
                .Where(c => c.SalonBranchId == salonBranchId)
                .Where(c => c.StaffId == e.SaleStaffId)
                .FirstOrDefault();
                if (service != default)
                {
                    e.CommsionValue = GetCommisionValue(service.CommissionValue, service.CommissionUnit, e);
                    e.CommisionValueAffterDiscount = GetCommisionValueAffterDiscount(service.CommissionValue, service.CommissionUnit, e);
                }

            });
            commissionArrangements.Where(e => e.ObjectType.Equals(INVOICEOBJECTTYPE.SERVICE)).ToList().ForEach(e => {
                //checkeck setting addvance TIMECOMMISION
                var service = commisionServiceDo
                .Where(c => c.ServiceId == e.ObjectId)
                .Where(c => c.SalonBranchId == salonBranchId)
                .Where(c => c.StaffId == e.ServiceStaffId).FirstOrDefault();
                if (service != default)
                {
                    e.CommsionServiceValue = GetCommisionValue(service.CommissionServiceValue, service.CommissionUnit, e);
                    e.CommisionServiceValueAffterDiscount = GetCommisionValueAffterDiscount(service.CommissionValue, service.CommissionUnit, e);
                }
            });
            _salon_hairContext.CommissionArrangement.UpdateRange(commissionArrangements);
            return _salon_hairContext.SaveChangesAsync();
        }
        private decimal? GetCommisionValueAffterDiscount(decimal commsionValue,string unit,CommissionArrangement commission)
        {
            if (unit.Equals(DISCOUNTUNIT.MONEY)) return commsionValue;

            return commission.ObjectPriceDiscount * commsionValue / 100;
        }
        private decimal? GetCommisionValue(decimal commsionValue, string unit, CommissionArrangement commission)
        {
            if (unit.Equals(DISCOUNTUNIT.MONEY)) return commsionValue;

            return commission.ObjectPrice * commsionValue / 100;
        }

        public IQueryable<CommissionArrangement> QueryForGetReport(string salonBranchIds, string start, string end, string keyword, long salonId)
        {
            var listsalonBranchIds = StringHelper.GetList<long>(salonBranchIds, ',');
            var dataRange = StringHelper.GetDateRangeQuery(start, end);
            var data = SearchAllFileds(keyword);
            data = data.Where(e => e.SalonId == salonId);
            data = data.Where(e => e.Invoice.PaymentStatus.Equals(PAYSTATUS.PAID));
            data = data.Where(e => e.Invoice.Status.Equals(OBJECTSTATUS.ENABLE));

            data = data.Where(e => e.Created.Value.Date >= dataRange.Item1.Date && e.Created.Value.Date <= dataRange.Item2.Date.Date);
            data = string.IsNullOrEmpty(salonBranchIds) ? data : data.Where(e => listsalonBranchIds.Contains(e.Invoice.SalonBranchId));
            return data;
        }
    }
}
    