

using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using SALON_HAIR_CORE.Repository;
using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace SALON_HAIR_CORE.Service
{
    public class CustomerPackageService: GenericRepository<CustomerPackage> ,ICustomerPackage
    {
        private salon_hairContext _salon_hairContext;
        public CustomerPackageService(salon_hairContext salon_hairContext) : base(salon_hairContext)
        {
            _salon_hairContext = salon_hairContext;
        }        
        public new void Edit(CustomerPackage customerPackage)
        {
            customerPackage.Updated = DateTime.Now;

            base.Edit(customerPackage);
        }
        public async new Task<int> EditAsync(CustomerPackage customerPackage)
        {            
            customerPackage.Updated = DateTime.Now;         
            return await base.EditAsync(customerPackage);
        }
        public new async Task<int> AddAsync(CustomerPackage customerPackage)
        {
            customerPackage.Created = DateTime.Now;
            return await base.AddAsync(customerPackage);
        }
        public new void Add(CustomerPackage customerPackage)
        {
            customerPackage.Created = DateTime.Now;
            base.Add(customerPackage);
        }
        public new void Delete(CustomerPackage customerPackage)
        {
            customerPackage.Status = "DELETED";
            base.Edit(customerPackage);
        }
        public new async Task<int> DeleteAsync(CustomerPackage customerPackage)
        {
            customerPackage.Status = "DELETED";
            return await base.EditAsync(customerPackage);
        }

        public async Task EditAsGenTransactionAsync(CustomerPackage customerPackage,long? salon_branch_id)
        {
           
            var oldData = _salon_hairContext.CustomerPackage.Where(e => e.Id == customerPackage.Id).AsNoTracking().FirstOrDefault();
            //if (customerPackage.NumberOfPaid < oldData.NumberOfPaid)
            //    throw new Exception("CAN'T_REDUCE_NUMBEROFPAID");
            //if (customerPackage.NumberOfUsed < oldData.NumberOfUsed)
            //    throw new Exception("CAN'T_REDUCE_NUMBEROFUSED");
            var listTransaction = new List<CustomerPackageTransaction>();
            if(customerPackage.NumberOfPaid != oldData.NumberOfPaid && customerPackage.NumberOfPaid != default)
            {
                var EXTENDtransaction = new CustomerPackageTransaction
                {
                    Action = CUSTOMERPACKAGETRANSACTIONACTION.EXTEND,
                    Created = DateTime.Now,
                    CreatedBy = oldData.CreatedBy,
                    CustomerId = oldData.CustomerId,
                    Day = oldData.Day,
                    Month = oldData.Month,
                    Year = oldData.Year,
                    PackageId = oldData.PackageId,
                    SalonBranchId = salon_branch_id,
                    Quantity =(int?)( customerPackage.NumberOfPaid - oldData.NumberOfPaid),
                    SalonId = customerPackage.SalonId,                    
                };
                listTransaction.Add(EXTENDtransaction);
            }
            if (customerPackage.NumberOfUsed != oldData.NumberOfUsed && customerPackage.NumberOfUsed != default)
            {
                var EXTENDtransaction = new CustomerPackageTransaction
                {
                    Action = CUSTOMERPACKAGETRANSACTIONACTION.DECREASE,
                    Created = DateTime.Now,
                    CreatedBy = oldData.CreatedBy,
                    CustomerId = oldData.CustomerId,
                    Day = oldData.Day,
                    Month = oldData.Month,
                    Year = oldData.Year,
                    PackageId = oldData.PackageId,
                    SalonBranchId = salon_branch_id,
                    Quantity = (int?)(customerPackage.NumberOfUsed - oldData.NumberOfUsed),
                    SalonId = customerPackage.SalonId,
                };
                listTransaction.Add(EXTENDtransaction);
            }
            _salon_hairContext.ChangeTracker.AutoDetectChangesEnabled = false;
            _salon_hairContext.CustomerPackageTransaction.AddRange(listTransaction);
          await  _salon_hairContext.SaveChangesAsync();
        }
    }
}
    