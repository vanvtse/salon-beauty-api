

using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_CORE.Interface;
using SALON_HAIR_CORE.Repository;
using System;
using System.Threading.Tasks;

namespace SALON_HAIR_CORE.Service
{
    public class CustomerPackageTransactionService: GenericRepository<CustomerPackageTransaction> ,ICustomerPackageTransaction
    {
        private salon_hairContext _salon_hairContext;
        public CustomerPackageTransactionService(salon_hairContext salon_hairContext) : base(salon_hairContext)
        {
            _salon_hairContext = salon_hairContext;
        }        
        public new void Edit(CustomerPackageTransaction customerPackageTransaction)
        {
            customerPackageTransaction.Updated = DateTime.Now;

            base.Edit(customerPackageTransaction);
        }
        public async new Task<int> EditAsync(CustomerPackageTransaction customerPackageTransaction)
        {            
            customerPackageTransaction.Updated = DateTime.Now;         
            return await base.EditAsync(customerPackageTransaction);
        }
        public new async Task<int> AddAsync(CustomerPackageTransaction customerPackageTransaction)
        {
            customerPackageTransaction.Created = DateTime.Now;
            return await base.AddAsync(customerPackageTransaction);
        }
        public new void Add(CustomerPackageTransaction customerPackageTransaction)
        {
            customerPackageTransaction.Created = DateTime.Now;
            base.Add(customerPackageTransaction);
        }
        public new void Delete(CustomerPackageTransaction customerPackageTransaction)
        {
            customerPackageTransaction.Status = "DELETED";
            base.Edit(customerPackageTransaction);
        }
        public new async Task<int> DeleteAsync(CustomerPackageTransaction customerPackageTransaction)
        {
            customerPackageTransaction.Status = "DELETED";
            return await base.EditAsync(customerPackageTransaction);
        }
    }
}
    