﻿using System;
using System.Collections.Generic;

namespace SALON_HAIR_ENTITY.Entities
{
    public partial class LogRespone
    {
        public long Id { get; set; }
        public string TraceIdentifier { get; set; }
        public string Data { get; set; }
        public DateTime? Created { get; set; }
        public string Method { get; set; }
        public string Path { get; set; }
        public string Tooken { get; set; }
    }
}
