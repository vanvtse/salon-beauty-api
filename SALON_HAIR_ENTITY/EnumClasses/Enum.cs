﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SALON_HAIR_ENTITY.Entities
{
    public static class INVOICEOBJECTTYPE
    {
        public const string PACKAGE = "PACKAGE";
        public const string PACKAGE_ON_BILL = "PACKAGE_ON_BILL";
        public const string SERVICE = "SERVICE";
        public const string PRODUCT = "PRODUCT";
        public const string EXTRA = "EXTRA";

    }
    public static class CUSTOMERPACKAGETRANSACTIONACTION
    {
        public readonly static string USE = "USE";
        public readonly static string PAY = "PAY";
        public const string DECREASE = "DECREASE";
        public const string EXTEND = "EXTEND";
    }
    public static class ORDERTYPE
    {
        public const string DESC = "DESC";
        public const string ASC = "ASC";
    }
    public static class OBJECTSTATUS
    {
        public readonly static string ENABLE = "ENABLE";
        public readonly static string CANCEL = "CANCEL";
        public readonly static string DISABLE = "DISABLE";
        public readonly static string DELETED = "DELETED";
    }
    public static class WAREHOUSETRANSATIONACTION
    {
        public readonly static string IMPORT = "IMPORT";
        public readonly static string EXPORT = "EXPORT";
        public readonly static string SALE = "SALE";
    }
    public static class PAYSTATUS
    {
        public const string UNPAID = "UNPAID";
        public const string PAID = "PAID";
        public const string CANCEL = "CANCEL";
    }
    public static class BOOKINGSTATUS
    {
        public const string CHECKIN = "CHECKIN";
        public const string CHECKOUT = "CHECKOUT";
        public const string NEW = "NEW";
        public const string CONFIRMED = "CONFIRMED";
        public const string PREPAID = "PREPAID";
    }
    public static class CLAIMUSER
    {
        public const string SUB = "sub";
        public const string ROLE = "role";
        public const string NAME = "name";
        public const string EMAILADDRESS = "emailAddress";
        public const string SALONID = "salonId";
        public const string EXP = "exp";
        public const string ISS = "iss";
        public const string AUD = "aud";
        public const string NAMEIDENTIFIER = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        public const string CURRENT_USER_ID = "userId";
    }
    public static class CASHBOOKTRANSACTIONACTION
    {
        public const string INCOME = "INCOME";
        public const string OUTCOME = "OUTCOME";
    }
    public static class DISCOUNTUNIT
    {
        public const string PERCENT = "PERCENT";
        public const string MONEY = "MONEY";
    }
    public static class SYSTEMDEFAULT
    {
        public const string PASSWORD = "Abc123456";
    }
    public static class CASH_BOOK_TRANSACTION_CATEGORY
    {
        public const string SALE = "SALE";
        public const string PREPAY = "PREPAY";
        public const string DEBT_RECOVERY = "DEBT_RECOVERY";
        public const string CASHBACK = "CASHBACK";
    }
    public static class GENERATECODE
    {
        public const string BOOKING = "ES";
        public const string CASHBOOKTRANSACTION = "ES";
        public const string INVOICE = "ES";
        public const string CUSTOMER = "KH";
        public const string FORMATSTRING = "000000";
    };
    public static class PAYMENT_METHOD
    {
        public const string CASH = "CASH";
        public const string BANK_TRANSFER = "BANK_TRANSFER";
        public const string SWIPE_CARD = "SWIPE_CARD";
        public const string OTHER = "OTHER";
        public const string DEBIT = "DEBIT";
    };
    public static class DEPT_BEHAVIOR
    {
        public const string PAY = "PAY";
        public const string DEBIT = "DEBIT";
    };
    public static class PREPAY_STATUS
    {
        public const string DEPOSITED = "DEPOSITED";
        public const string NOT_DEPOSITED = "NOT_DEPOSITED";
    };
    public static class FORMART_STRING
    {
        public const string DATER_REPORT = "dd/MM/yyyy";
    }
    public static class MysqlExcetpionMessage
    {
        public static List<MessageReference> messageReferences = new List<MessageReference> {
            new MessageReference {Number=1053,SqlState="08S01",Symbol="ER_SERVER_SHUTDOWN"},
            new MessageReference {Number=1054,SqlState="42S22",Symbol="ER_BAD_FIELD_ERROR"},
            new MessageReference {Number=1055,SqlState="42000",Symbol="ER_WRONG_FIELD_WITH_GROUP"},
            new MessageReference {Number=1056,SqlState="42000",Symbol="ER_WRONG_GROUP_FIELD"},
            new MessageReference {Number=1057,SqlState="42000",Symbol="ER_WRONG_SUM_SELECT"},
            new MessageReference {Number=1058,SqlState="21S01",Symbol="ER_WRONG_VALUE_COUNT"},
            new MessageReference {Number=1059,SqlState="42000",Symbol="ER_TOO_LONG_IDENT"},
            new MessageReference {Number=1060,SqlState="42S21",Symbol="ER_DUP_FIELDNAME"},
            new MessageReference {Number=1061,SqlState="42000",Symbol="ER_DUP_KEYNAME"},
            new MessageReference {Number=1062,SqlState="23000",Symbol="ER_DUP_ENTRY"},
            new MessageReference {Number=1063,SqlState="42000",Symbol="ER_WRONG_FIELD_SPEC"},
        };
        public static List<IndexRefernce> indexRefernces = new List<IndexRefernce> {
            new IndexRefernce {Index = "customer_phone_unique", Description ="CUSTOMER_PHONE" },
            new IndexRefernce {Index = "salon_email_unique", Description ="SALON_EMAIL" },
            new IndexRefernce {Index = "user_email_unique", Description ="USER_EMAIL" },
        };
    }
    public class MessageReference
    {
        public int Number { get; set; }
        public string SqlState { get; set; }
        public string Symbol { get; set; }
    }
    public class IndexRefernce {
        public string Index { get; set; }
        public string Description { get; set; }
    }
}
