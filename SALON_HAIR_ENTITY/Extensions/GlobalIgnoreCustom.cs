﻿using Microsoft.EntityFrameworkCore;
using SALON_HAIR_ENTITY.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SALON_HAIR_ENTITY.Entities
{
    public static class GlobalIgnoreCustom
    {
        public static ModelBuilder BuilCustomFillter(ModelBuilder builder)
        {
            builder.Entity<Customer>().Ignore(nameof(Invoice));
            builder.Entity<Customer>().Ignore(e => e.Booking);
            builder.Entity<Booking>().Ignore(e => e.Customer);
            return builder;
        }
    }
}
