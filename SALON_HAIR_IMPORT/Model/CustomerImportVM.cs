﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SALON_HAIR_IMPORT.Model
{
    public class CustomerImportVM
    {
        public string CustomerSource { get; set; }
        public string Address { get; set; }
        public DateTime? Dob { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Sex { get; set; }    
        public decimal? Debt { get; set; }
        public string PackageName { get; set; }
        public string PackagePaid { get; set; }
        public string PackageUsed { get; set; }
       
    }
}
