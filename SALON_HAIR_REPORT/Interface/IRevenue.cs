﻿using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_REPORT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SALON_HAIR_REPORT.Interface
{
    /// <summary>
    /// Doanh thu Interface
    /// </summary>
    public interface IRevenue
    {
        /// <summary>
        /// Doanh thu theo phương thức thanh toán - chart
        /// </summary>
        IEnumerable<RevenuePaymentMethodGeneralVM> GetRevenuePaymentMethodGeneralVM(long salonId, string salonBranchIds = "", string paymentMethodCodes = "", string start = "", string end = "");
        /// <summary>
        /// Doanh thu theo phương thức thanh toán chi tiết
        /// </summary>
        IEnumerable<RevenuePaymentMethodDetailVM> GetRevenuePaymentMethodDetailVM(long salonId,string salonBranchIds = "", string paymentMethodCodes = "", string start = "", string end = "");
        /// <summary>
        /// Doanh thu theo dịch vụ chi tiết
        /// </summary>
        /// <param name="salonBranchIds"></param>
        /// <param name="serviceIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        IEnumerable<RevenueServiceDetailVM> GetRevenueServiceDetail(long salonId,string salonBranchIds = "", string serviceIds = "", string start = "", string end = "", string keyword = "");
        /// <summary>
        /// Doanh thu theo sản phẩm chi tiết
        /// </summary>
        /// <param name="salonBranchIds"></param>
        /// <param name="serviceIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        IEnumerable<RevenueProductDetailVM> GetRevenueProductDetail(long salonId, string salonBranchIds, string productIds, string start, string end, string keyword);
        /// <summary>
        /// Doanh thu theo hóa đơn chi tiết
        /// </summary>
        /// <param name="salonBranchIds"></param>
        /// <param name="serviceIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        IEnumerable<RevenueInvoiceDetailVM> GetRevenueInvoiceDetail(long salonId, string salonBranchIds, string start, string end, string keyword);
        /// <summary>
        /// Doanh thu theo thời gian
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        IEnumerable<RevenueDateDetailVM> GetRevenueDateDetail(long salonId, string salonBranchIds, string start, string end, string keyword, string type);
        /// <summary>
        /// Tần suất khách hàng
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        IEnumerable<CustomerFrequencyVM> GetCustomerFrequency(long salonId, string salonBranchIds, string start, string end, string keyword, string type);
       
        /// <summary>
        /// Báo cáo thu chi
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <param name="action"></param>
        /// <param name="cash_book_transaction_category_id"></param>
        /// <returns></returns>
        IQueryable<RevenueExpenditureVM> GetRevenueExpenditure(long salonId, string salonBranchIds, string start, string end, string keyword, string action, string cash_book_transaction_category_id);

        IQueryable<CashBookTransaction> GetCashBookTransaction(long salonId, string salonBranchIds, string start, string end, string keyword, string action, string cash_book_transaction_category_id);

    }
}
