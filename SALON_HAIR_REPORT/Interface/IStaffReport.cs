﻿using SALON_HAIR_REPORT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SALON_HAIR_REPORT.Interface
{
    public interface IStaffReport
    {
        /// <summary>
        /// Hoa Hồng nhân viên
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        IEnumerable<StaffCommissionTmp> GetCommissionStaff(long salonId,string salonBranchIds = "", string start = "", string end = "");
        IEnumerable<CommissionStaffDetailVM> GetCommissionStaffDetail(long salonId, long staffId, string start, string end);

        /// <summary>
        /// Chi tiết hoa Hồng nhân viên theo nhân viên
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>

    }
}
