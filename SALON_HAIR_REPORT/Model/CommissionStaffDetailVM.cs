﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class CommissionStaffDetailVM
    {
        [Display(Name = "Mã hóa đơn")]
        public string InvoiceCode { get; set; }
        [Display(Name = "Thời gian")]
        public DateTime? Time  { get; set; }
        [Display(Name = "Tên khách hàng")]
        public string CustomerName { get; set; }
        [Display(Name = "Dịch vụ/Sản Phẩm/Gói Dịch vụ")]
        public string Name { get; set; }
        [Display(Name = "Hoa hồng bán hàng")]
        public decimal? SaleCommsion { get; set; }
        [Display(Name = "Hoa hồng dịch vụ")]
        public decimal? ServiceCommsion { get; set; }
        [Display(Name = "Định danh hóa đơn")]
        public long InvoiceId { get; set; }

    }
}
