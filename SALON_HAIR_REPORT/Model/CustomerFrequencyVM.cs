﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class CustomerFrequencyVM
    {
        [Display(Name = "Thời gian")]
        public string Time { get; set; }
        [Display(Name = "Số lượng khách")]
        public int CustomerQuanity { get; set; }
        [Display(Name = "Doanh thu")]
        public decimal? Revenue { get; set; }       
    }
}
