﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class RevenueDateDetailVM
    {
        [Display(Name = "Thời gian")]
        public string Time { get; set; }
        [Display(Name = "Số lượng đơn hàng")]
        public int InvoiceQuanity { get; set; }
        [Display(Name = "Tổng tiền")]
        public decimal? Revenue { get; set; }
        [Display(Name = "Tổng chiết khấu")]
        public decimal? Discount { get; set; }
        [Display(Name = "Hoa Hồng")]
        public decimal? Commision { get; set; }
        [Display(Name = "Đã thanh toán")]
        public decimal? PaidTotal { get; set; }   
        [Display(Name = "Ghi nợ")]
        public decimal? Debit { get; set; }
        [Display(Name = "Tổng doanh thu")]
        public decimal? TotalRevenue { get; set; }
    }
}
