﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class RevenueExpenditureVM
    {
        [Display(Name = "Ngày tạo")]
        public DateTime Date { get; set; }
        [Display(Name = "Mã phiếu")]
        public string Code { get; set; }
        [Display(Name = "Người nhận/nộp")]
        public string Cashier { get; set; }
        [Display(Name = "Lý do")]
        public string Description { get; set; }
        [Display(Name = "PTTT")]
        public string PaymentMethod { get; set; }
        [Display(Name = "Số tiền")]
        public decimal? Total { get; set; }
        [Display(Name = "Loại phiếu")]
        public string Action { get; set; }
        [Display(Name = "Định danh mã phiếu")]
        public long CashbookId { get; set; }
    }

}
