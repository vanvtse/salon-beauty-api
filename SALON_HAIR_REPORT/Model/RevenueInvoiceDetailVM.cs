﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class RevenueInvoiceDetailVM
    {
        [Display(Name = "Mã hóa đơn")]
        public string InvoiceCode { get; set; }
        [Display(Name = "Tên khách hàng")]
        public string CustomerName { get; set; }
        [Display(Name = "Doanh thu")]
        public decimal Revenue { get; set; }
        [Display(Name = "Chiết khấu")]
        public decimal? Discount { get; set; }
        [Display(Name = "Đã thanh toán")]
        public decimal? PaidRevenue { get; set; }
        [Display(Name = "Tiền mặt")]
        public decimal? Cash { get; set; }
        [Display(Name = "Chuyển khoản")]
        public decimal? BankTransfer { get; set; }
        [Display(Name = "Quẹt thẻ")]
        public decimal? SwipeCard { get; set; }
        [Display(Name = "Khác")]
        public decimal? Other { get; set; }
        [Display(Name = "Tiền thối lại")]
        public decimal? CashBack { get; set; }
        [Display(Name = "Ghi nợ")]
        public decimal? Debit { get; set; }
        [Display(Name = "Ghi chú")]
        public string Note { get; set; }
        [Display(Name ="Số định danh")]
        public long InvoiceId { get; set; }
    }
}
