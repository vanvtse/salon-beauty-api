﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class RevenuePaymentMethodDetailVM
    {
        [Display(Name = "Ngày")]
        public string Date { get; set; }
        [Display(Name = "Tiền mặt")]
        public decimal? Cash { get; set; }
        [Display(Name = "Quẹt thẻ")]
        public decimal? SwipeCard { get; set; }
        [Display(Name = "Chuyển khoản")]
        public decimal? BankTransfer { get; set; }
        [Display(Name = "Ghi nợ")]
        public decimal? Debit { get; set; }
        [Display(Name = "Thu Khác")]
        public decimal? Other { get; set; }
        [Display(Name = "Tổng doanh thu")]
        public decimal? Total { get; set; }
    }
}
