﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class RevenuePaymentMethodGeneralVM
    {
        public string Name { get; set; }
        public IEnumerable<DataItem> Data { get; set; }
        public decimal Number { get; set; }
    }
    public class DataItem
    {
        public string Label { get; set; }
        public decimal? Number { get; set; }
    }
}
