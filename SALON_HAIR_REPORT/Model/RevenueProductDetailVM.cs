﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class RevenueProductDetailVM
    {
        [Display(Name = "Tên sản phẩm")]
        public string ProductName { get; set; }
        [Display(Name = "Số lượng bán")]
        public decimal? SaleQuantity { get; set; }
        [Display(Name = "Đơn giá")]
        public decimal? UnitPrice { get; set; }
        [Display(Name = "Tổng tiền")]
        public decimal? Total { get; set; }
        [Display(Name = "Giảm giá")]
        public decimal? Discount { get; set; }
        [Display(Name = "Doanh thu")]
        public decimal? Revenue { get; set; }
    }
}
