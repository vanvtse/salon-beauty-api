﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class StaffCommissionVM
    {
        [Display(Name = "Nhân viên")]
        public string StaffName { get; set; }
        [Display(Name = "Hoa hồng dịch vụ")]
        public decimal? ServiceCommission { get; set; }
        [Display(Name = "Hoa hồng bán hàng")]
        public decimal? SaleCommission { get; set; }
        [Display(Name = "Tổng hoa hồng")]
        public decimal? SumCommission { get; set; }
    }
    public class StaffCommissionTmp
    {
        [Display(Name = "Nhân viên")]
        public string StaffName { get; set; }
        [Display(Name = "Hoa hồng dịch vụ")]
        public decimal? ServiceCommission { get; set; }
        [Display(Name = "Hoa hồng bán hàng")]
        public decimal? SaleCommission { get; set; }
        [Display(Name = "Tổng hoa hồng")]
        public decimal? SumCommission { get; set; }
        [Display(Name = "Định danh nhân viên")]
        public long? StaffId { get; set; }
    }
}
