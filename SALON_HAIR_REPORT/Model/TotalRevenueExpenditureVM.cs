﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SALON_HAIR_REPORT.Model
{
    public class TotalRevenueExpenditureVM
    {
        public decimal? TotalRevenue { get; set; }
        public decimal? TotalExpenditure { get; set; }
    }
}
