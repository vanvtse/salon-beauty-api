﻿using Microsoft.EntityFrameworkCore;
using SALON_HAIR_CORE.Interface;
using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_REPORT.Interface;
using SALON_HAIR_REPORT.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ULTIL_HELPER;

namespace SALON_HAIR_REPORT.Service
{
    public class RevenueService : IRevenue
    {
        private salon_hairContext _salon_hairContext;
        private IInvoiceDetail _invoiceDetail;
        private IInvoice _invoice;
        private ICashBookTransaction _cashBookTransaction;
        public RevenueService(ICashBookTransaction cashBookTransaction,IInvoice invoice, salon_hairContext salon_hairContext, IInvoiceDetail invoiceDetail)
        {
            _invoice = invoice;
            _invoiceDetail = invoiceDetail;
            _salon_hairContext = salon_hairContext;
            _cashBookTransaction = cashBookTransaction;
        }
        /// <summary>
        /// Doanh thu theo phương thức thanh toán chi tiết
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="paymentMethodCodes"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<RevenuePaymentMethodDetailVM> GetRevenuePaymentMethodDetailVM(long salonId, string salonBranchIds = "", string paymentMethodCodes = "", string start = "", string end = "")
        {
            var listsalonBranchIds = StringHelper.GetList<long>(salonBranchIds, ',');
            var listPaymentMethodCodes = StringHelper.GetList<string>(paymentMethodCodes, ',');
            var dataRange = StringHelper.GetDateRangeQuery(start, end);

            var data = _salon_hairContext.InvoicePayment.Where(e => e.SalonId == salonId);
            data = data.Where(e => e.Invoice.PaymentStatus.Equals(PAYSTATUS.PAID));
            data = data.Where(e => e.Created.Value.Date >= dataRange.Item1.Date && e.Created.Value.Date <= dataRange.Item2.Date.Date);

            data = string.IsNullOrEmpty(salonBranchIds) ? data : data.Where(e => listsalonBranchIds.Contains(e.SalonBranchId.Value));
            data = string.IsNullOrEmpty(paymentMethodCodes) ? data : data.Where(e => listPaymentMethodCodes.Contains(e.InvoiceMethod.Code));
            data = data.Include(e => e.InvoiceMethod);
            data = data.Include(e => e.Invoice);
            var process = data.ToList();
            var listBookingId = process.Select(e => e.Invoice.BookingId);
            var prepayPayment = _salon_hairContext.BookingPrepayPayment.Where(e => listBookingId.Contains(e.BookingId)).Include(e => e.BookingMethod).ToList();
            var dataReturn = process.GroupBy(e => e.Created.Value.ToString(FORMART_STRING.DATER_REPORT))
                .Select(e => new RevenuePaymentMethodDetailVM
                {
                    Date = e.Key,
                    BankTransfer = e.Where(c => c.InvoiceMethod.Code.Equals(PAYMENT_METHOD.BANK_TRANSFER)).Sum(c => c.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.BANK_TRANSFER),
                    SwipeCard = e.Where(c => c.InvoiceMethod.Code.Equals(PAYMENT_METHOD.SWIPE_CARD)).Sum(c => c.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.SWIPE_CARD),
                    Cash = e.Where(c => c.InvoiceMethod.Code.Equals(PAYMENT_METHOD.CASH)).Sum(c => c.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.CASH),
                    Debit = e.Where(c => c.InvoiceMethod.Code.Equals(PAYMENT_METHOD.DEBIT)).Sum(c => c.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.DEBIT),
                    Other = e.Where(c => c.InvoiceMethod.Code.Equals(PAYMENT_METHOD.OTHER)).Sum(c => c.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.OTHER),
                    Total = e.Sum(c => c.Total)
                });
            return dataReturn;
        }     
        /// <summary>
        /// Doanh thu theo phương thức thanh toán - chart
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="paymentMethodCodes"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<RevenuePaymentMethodGeneralVM> GetRevenuePaymentMethodGeneralVM(long salonId, string salonBranchIds = "", string paymentMethodCodes = "", string start = "", string end = "")
        {
            var listsalonBranchIds = StringHelper.GetList<long>(salonBranchIds, ',');
            var listPaymentMethodCodes = StringHelper.GetList<string>(paymentMethodCodes, ',');
            var dataRange = StringHelper.GetDateRangeQuery(start, end);
            //var data = _invoicePayment.GetAll();
            var data = _salon_hairContext.InvoicePayment.Where(e => e.SalonId == salonId);
            //data = GetByCurrentSalon(data);
            data = data.Where(e => e.Invoice.PaymentStatus.Equals(PAYSTATUS.PAID));
            data = data.Where(e => e.Created.Value.Date >= dataRange.Item1.Date && e.Created.Value.Date <= dataRange.Item2.Date.Date);
            data = string.IsNullOrEmpty(salonBranchIds) ? data : data.Where(e => listsalonBranchIds.Contains(e.SalonBranchId.Value));
            data = string.IsNullOrEmpty(paymentMethodCodes) ? data : data.Where(e => listPaymentMethodCodes.Contains(e.InvoiceMethod.Code));
            var dataCau = data.Include(e => e.InvoiceMethod).Include(e=>e.Invoice).ToList();

            var listBookingId = dataCau.Select(e => e.Invoice.BookingId);
            var prepayPayment = _salon_hairContext.BookingPrepayPayment.Where(e => listBookingId.Contains(e.BookingId)).Include(e => e.BookingMethod).ToList();



            dataCau = dataCau.Union(GenInvoicePayments(GetDatesBetween(dataRange.Item1, dataRange.Item2), salonId)).ToList();
            dataCau = dataCau.OrderBy(e => e.Created).ToList();
            var rs = (from a in dataCau
                      group a by a.InvoiceMethod.Code into b
                      select new RevenuePaymentMethodGeneralVM
                      {
                          Name = b.Key,
                          Data = b.AsQueryable().GroupBy(e => e.Created.Value.ToString(FORMART_STRING.DATER_REPORT)).
                          Select(x => new DataItem
                          {
                              Label = x.Key,
                              Number = x.Sum(e => e.Total.Value) + GetPrePayment(prepayPayment,x.Key)
                          }),
                          Number = b.Sum(e => e.Total.Value)
                      }).ToList();
            //Sum catulate
            var sum = new RevenuePaymentMethodGeneralVM
            {
                Name = "SUM",
                Number = dataCau.Sum(e => e.Total.Value),
                Data = dataCau.GroupBy(e => e.Created.Value.ToString(FORMART_STRING.DATER_REPORT)).
                    Select(x => new DataItem
                    {
                        Label = x.Key,
                        Number = x.Sum(e => e.Total.Value) + GetPrePayment(prepayPayment, x.Key)
                    }),
            };
            rs.Add(sum);
            return rs;
        }
        /// <summary>
        /// Doanh thu theo dịch vụ
        /// </summary>
        /// <param name="salonBranchIds"></param>
        /// <param name="serviceIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public IEnumerable<RevenueServiceDetailVM> GetRevenueServiceDetail(long salonId, string salonBranchIds = "", string serviceIds = "", string start = "", string end = "", string keyword = "")
        {
            var data = GetinvoiceDetails(salonId, INVOICEOBJECTTYPE.SERVICE, salonBranchIds, serviceIds, start, end, keyword);
            var result = data.ToList().GroupBy(e => e.ObjectId).Select(e => new RevenueServiceDetailVM
            {
                ServiceName = e.FirstOrDefault().ObjectName,
                Discount = e.Sum(c => c.TotalExcludeDiscount - c.TotalIncludeDiscount),
                Revenue = e.Sum(c => c.TotalExcludeDiscount),
                SaleQuantity = e.Count(),
                Total = e.Sum(c => c.TotalIncludeDiscount),
                UnitPrice = e.FirstOrDefault().ObjectPrice
            });
            return result;
        }
        /// <summary>
        /// Doanh thu theo sản phẩm
        /// </summary>
        /// <param name="salonBranchIds"></param>
        /// <param name="serviceIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public IEnumerable<RevenueProductDetailVM> GetRevenueProductDetail(long salonId, string salonBranchIds, string productIds, string start, string end, string keyword)
        {
            var data = GetinvoiceDetails(salonId, INVOICEOBJECTTYPE.PRODUCT, salonBranchIds, productIds, start, end, keyword);
            var result = data.ToList().GroupBy(e => e.ObjectId).Select(e => new RevenueProductDetailVM
            {
                ProductName = e.FirstOrDefault()?.ObjectName,
                Discount = e.Sum(c => c.TotalExcludeDiscount - c.TotalIncludeDiscount),
                Revenue = e.Sum(c => c.TotalExcludeDiscount),
                SaleQuantity = e.Count(),
                Total = e.Sum(c => c.TotalIncludeDiscount),
                UnitPrice = e.FirstOrDefault()?.ObjectPrice
            });
            return result;
        }
        /// <summary>
        /// Doanh thu theo hóa đơn chi tiết
        /// </summary>
        /// <param name="salonBranchIds"></param>
        /// <param name="serviceIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public IEnumerable<RevenueInvoiceDetailVM> GetRevenueInvoiceDetail(long salonId, string salonBranchIds, string start, string end, string keyword)
        {
            var data = _invoice.QueryForGetReport(salonBranchIds, start, end, keyword, salonId);
            data = data.Include(e => e.Customer);
            data = data.Include(e => e.CommissionArrangement);
            data = data.Include(e => e.InvoicePayment).ThenInclude(e => e.InvoiceMethod);
           
            data = OrderBy(data, "");
            var dataProcess = data.ToList();
            var listBookingId = dataProcess.Select(e => e.BookingId);
            var prepayPayment = _salon_hairContext.BookingPrepayPayment.Where(e => listBookingId.Contains(e.BookingId)).Include(e=>e.BookingMethod).ToList();
            var reportByInvoiceDetailVM = from invoice in dataProcess
                                          select new RevenueInvoiceDetailVM
                                          {
                                              InvoiceCode = invoice.Code,
                                              CustomerName = invoice.Customer?.Name ?? "Khách vãng lai",
                                              Revenue = invoice.TotalDetails,
                                              Discount = invoice.DiscountTotal,
                                              PaidRevenue = invoice.Total - invoice.InvoicePayment.Where(e => e.InvoiceMethod.Code.Equals(PAYMENT_METHOD.DEBIT)).Sum(e => e.Total),
                                              BankTransfer = invoice.InvoicePayment.Where(e => e.InvoiceMethod.Code.Equals(PAYMENT_METHOD.BANK_TRANSFER)).Sum(e => e.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.BANK_TRANSFER,invoice.BookingId),
                                              SwipeCard = invoice.InvoicePayment.Where(e => e.InvoiceMethod.Code.Equals(PAYMENT_METHOD.SWIPE_CARD)).Sum(e => e.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.SWIPE_CARD, invoice.BookingId),
                                              Debit = invoice.InvoicePayment.Where(e => e.InvoiceMethod.Code.Equals(PAYMENT_METHOD.DEBIT)).Sum(e => e.Total) ,
                                              Other = invoice.InvoicePayment.Where(e => e.InvoiceMethod.Code.Equals(PAYMENT_METHOD.OTHER)).Sum(e => e.Total) + GetPrePayment(prepayPayment, PAYMENT_METHOD.OTHER, invoice.BookingId),
                                              Cash = invoice.InvoicePayment.Where(e => e.InvoiceMethod.Code.Equals(PAYMENT_METHOD.CASH)).Sum(e => e.Total) - (invoice.CashBack > 0? invoice.CashBack : 0) + GetPrePayment(prepayPayment, PAYMENT_METHOD.CASH, invoice.BookingId),
                                              Note = invoice.NotePayment,
                                              CashBack = invoice.CashBack,
                                              InvoiceId = invoice.Id
                                          };
            return reportByInvoiceDetailVM;
        }
        /// <summary>
        /// Doanh thu theo thời gian
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<RevenueDateDetailVM> GetRevenueDateDetail(long salonId, string salonBranchIds, string start, string end, string keyword, string type)
        {

            //MM/dd/yyyy/HH
            var data = _invoice.QueryForGetReport(salonBranchIds, start, end, keyword, salonId);
            data = data.Include(e => e.CommissionArrangement);
            data = data.Include(e => e.InvoicePayment).ThenInclude(c => c.InvoiceMethod);
            var process = data.ToList().GroupBy(e => GetStringGroupByDate(e.Created.Value, type));
            List<RevenueDateDetailVM> result = new List<RevenueDateDetailVM>();
            var dateRange = StringHelper.GetDateRangeQuery(start, end);
            foreach (var item in GetDatesBetween(dateRange.Item1, dateRange.Item2, type))
            {
                result.Add(new RevenueDateDetailVM
                {
                    Time = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Key ?? item,
                    InvoiceQuanity = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Count()??0,
                    Revenue = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Sum(a => a.TotalDetails) ?? 0,
                    Discount = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Sum(a => a.DiscountTotal) ?? 0,
                    PaidTotal = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Sum(a => a.PaidTotal ) ?? 0,
                    Debit = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Sum(c => c.InvoicePayment.Where(v => v.InvoiceMethod.Code.Equals(PAYMENT_METHOD.DEBIT)).Sum(z => z.Total)) ?? 0,
                    Commision = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Sum(c => c.CommissionArrangement.Sum(x => x.CommsionServiceValue + x.CommsionValue)) ?? 0,
                    TotalRevenue = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Sum(a => a.Total - a.CommissionArrangement.Sum(c=>c.CommsionServiceValue + c.CommsionValue)) ?? 0
                });
            }
            return result;
        }
        /// <summary>
        /// Tần suất khách hàng
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<CustomerFrequencyVM> GetCustomerFrequency(long salonId, string salonBranchIds, string start, string end, string keyword, string type)
        {
            var data = _invoice.QueryForGetReport(salonBranchIds, start, end, keyword, salonId);
            var process = data.ToList().GroupBy(e => GetStringGroupByDate(e.Created.Value, type));
            List<CustomerFrequencyVM> result = new List<CustomerFrequencyVM>();
            var dateRange = StringHelper.GetDateRangeQuery(start, end);
            foreach (var item in GetDatesBetween(dateRange.Item1, dateRange.Item2,type))
            {
                result.Add(new CustomerFrequencyVM {
                    Time = process.Where(e=>e.Key.Equals(item)).FirstOrDefault()?.Key??item,
                    CustomerQuanity = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Count()??0,
                    Revenue = process.Where(e => e.Key.Equals(item)).FirstOrDefault()?.Sum(v=>v.Total)??0
                });
            }
            return result;
        }
        /// <summary>
        /// Báo cáo thu chi
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="keyword"></param>
        /// <param name="action"></param>
        /// <param name="cash_book_transaction_category_id"></param>
        /// <returns></returns>
        public IQueryable<CashBookTransaction> GetCashBookTransaction(long salonId, string salonBranchIds, string start, string end, string keyword, string action, string cash_book_transaction_category_id)
        {
            var cash_book_transaction_category_ids = StringHelper.GetList<long>(cash_book_transaction_category_id, ',');
            var data = _cashBookTransaction.QueryForGetReport(salonBranchIds, start, end, keyword, salonId);
            data = string.IsNullOrEmpty(action) ? data : data.Where(e => e.Action.Equals(action));
            data = string.IsNullOrEmpty(cash_book_transaction_category_id) ? data : data.Where(e => cash_book_transaction_category_ids.Contains(e.CashBookTransactionCategoryId.Value));
            return data;
           
        }
        private string GetStringGroupByDate(DateTime dateTime, string type)
        {
            switch (type)
            {
                case "HOUR":
                    return dateTime.ToString("MM/dd/yyyy/HH");
                case "HOUR-GROUP":
                    return dateTime.ToString("HH:00");
                case "DAY":
                    return dateTime.ToString("MM/dd/yyyy");
                case "DAY-GROUP":
                    return dateTime.ToString("MM/dd/yyyy");
                case "DAYOFWEEK":
                    return dateTime.ToString("ddd/MM/yyyy");
                case "DAYOFWEEK-GROUP":
                    return dateTime.ToString("ddd");
                case "WEEK":
                    return dateTime.ToString("ddd/yyyy");
                case "MONTH-GROUP":
                    return "Tháng "+dateTime.ToString("MM");
                case "WEEK-GROUP":                    
                      return  "Tuần thứ "+GetIso8601WeekOfYear(dateTime).ToString("00");                   
                default:
                    return dateTime.ToString("MM/yyyy");
            }
        }
        private string GetStringOrderByDate(DateTime dateTime, string type)
        {

            switch (type)
            {
                case "HOUR":
                    return dateTime.ToString("MM/dd/yyyy/HH");
                case "HOUR-GROUP":
                    return dateTime.ToString("HH");
                case "DAY":
                    return dateTime.ToString("MM/dd/yyyy");
                case "DAY-GROUP":
                    return dateTime.ToString("dd");
                case "DAYOFWEEK":
                    return dateTime.ToString("ddd/MM/yyyy");
                case "DAYOFWEEK-GROUP":
                    return GetDateOfWeekValue(dateTime.DayOfWeek).ToString();
                case "WEEK":
                    return dateTime.ToString("ddd/yyyy");
                case "MONTH-GROUP":
                    return dateTime.ToString("MM");
                default:
                    return dateTime.ToString("MM/yyyy");
            }
        }
        private int GetDateOfWeekValue(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Friday:
                    return 5;
                case DayOfWeek.Monday:
                    return 1;
                case DayOfWeek.Saturday:
                    return 6;
                case DayOfWeek.Sunday:
                    return 7;
                case DayOfWeek.Thursday:
                    return 4;
                case DayOfWeek.Tuesday:
                    return 2;
                case DayOfWeek.Wednesday:
                    return 3;
                default:
                    return 7;
            }
        }
        private IQueryable<InvoiceDetail> GetinvoiceDetails(long salonId, string objectType, string salonBranchIds = "", string serviceIds = "", string start = "", string end = "", string keyword = "")
        {
            var listsalonBranchIds = StringHelper.GetList<long>(salonBranchIds, ',');
            var listServiceIds = StringHelper.GetList<long>(serviceIds, ',');
            var dataRange = StringHelper.GetDateRangeQuery(start, end);
            var data = _invoiceDetail.SearchAllFileds(keyword);

            data = data.Where(e => e.SalonId == salonId);
            data = data.Where(e => e.ObjectType.Equals(objectType));
            data = data.Where(e => e.Invoice.PaymentStatus.Equals(PAYSTATUS.PAID));
            data = data.Where(e => e.Status.Equals(OBJECTSTATUS.ENABLE));
            data = data.Where(e => e.Created.Value.Date >= dataRange.Item1.Date && e.Created.Value.Date <= dataRange.Item2.Date.Date);
            data = string.IsNullOrEmpty(salonBranchIds) ? data : data.Where(e => listsalonBranchIds.Contains(e.SalonBranchId.Value));
            data = string.IsNullOrEmpty(serviceIds) ? data : data.Where(e => listServiceIds.Contains(e.ObjectId.Value));

            return data;
        }
        private IQueryable<InvoicePayment> GetByCurrentSalon(IQueryable<InvoicePayment> data, long salonId)
        {
            data = data.Where(e => e.SalonId == salonId);
            return data;
        }
        private List<DateTime> GetDatesBetween(DateTime startDate, DateTime endDate)
        {
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                allDates.Add(date);
            return allDates;

        }
        private IEnumerable<InvoicePayment> GenInvoicePayments(List<DateTime> dateTimes, long salonId)
        {

            //var salonId = 1;//JwtHelper.GetCurrentInformationLong(User, x => x.Type.Equals(CLAIMUSER.SALONID));

            var paymentMethods = _salon_hairContext.PaymentMethod.Where(e => e.SalonId == salonId).ToList();
            var list = new List<InvoicePayment>();
            dateTimes.ForEach(e =>
            {
                paymentMethods.ForEach(c =>
                {
                    list.Add(new InvoicePayment { Created = e, Total = 0, InvoiceMethod = new PaymentMethod { Code = c.Code } });
                });

            });
            return list;
        }
        private string GetStringDateimeFormat(string type)
        {
            switch (type)
            {
                case "HOUR":
                    return "HH:00 dd/MM/yyyy";
                case "HOUR-GROUP":
                    return "HH:00";
                case "DAY":
                    return "MM/dd/yyyy";
                case "DAY-GROUP":
                    return "dd";
                case "DAYOFWEEK":
                    return "ddd/MM/yyyy";
                case "DAYOFWEEK-GROUP":
                    return "ddd";
                case "WEEK":
                    return "ddd/yyyy";
                case "MONTH-GROUP":
                    return "MM";
                default:
                    return "MM/yyyy";
            }
        }
        private IQueryable<Invoice> OrderBy(IQueryable<Invoice> data, string orderType)
        {
            if (orderType.Equals("asc"))
            {
                data = data.OrderBy(e => e.Id);
            }
            else
            {
                data = data.OrderByDescending(e => e.Id);
            }
            return data;
        }
        private List<string> GetDatesBetween(DateTime startDate, DateTime endDate,string type)
        {
            var rs = new List<string>();
            switch (type)
            {
                case "HOUR":                  
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                        rs.Add(date.ToString("MM/dd/yyyy/HH"));
                    return rs;

                case "HOUR-GROUP":
                    for (int i = 0; i < 24; i++)
                    {
                        rs.Add(i.ToString("00")+":00");
                    }
                    return rs;                   
                case "DAY":
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                        rs.Add(date.ToString("MM/dd/yyyy"));
                    return rs;
                case "DAY-GROUP":
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                        rs.Add(date.ToString("MM/dd/yyyy"));
                    return rs;
                case "DAYOFWEEK":
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                        rs.Add(date.ToString("ddd/MM/yyyy"));
                    return rs;
                case "DAYOFWEEK-GROUP":
                    DateTime today = DateTime.Today;
                    int currentDayOfWeek = (int)today.DayOfWeek;
                    DateTime sunday = today.AddDays(-currentDayOfWeek);
                    DateTime monday = sunday.AddDays(1);
                    // If we started on Sunday, we should actually have gone *back*
                    // 6 days instead of forward 1...
                    if (currentDayOfWeek == 0)
                    {
                        monday = monday.AddDays(-7);
                    }
                    var dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();
                    rs = dates.Select(e => e.ToString("ddd")).ToList();
                    return rs;
                case "WEEK":
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(7))
                        rs.Add(date.ToString("ddd/yyyy"));
                    return rs;
                case "WEEK-GROUP":
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(7))
                        rs.Add("Tuần thứ "+GetIso8601WeekOfYear(date).ToString("00"));
                    return rs;
                case "MONTH-GROUP":
                    for (DateTime date = startDate; date <= endDate; date = date.AddMonths(1))
                        rs.Add("Tháng "+date.ToString("MM"));
                    return rs;
                default:
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                        rs.Add(date.ToString("MM/dd/yyyy"));
                    return rs;
            }

        }
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        private decimal GetPrePayment(List<BookingPrepayPayment> bookingPrepayPayments,string paymentMethodCode,long? bookingId)
        {
            return bookingPrepayPayments.Where(e => e.BookingMethod.Code.Equals(paymentMethodCode)).Where(e=>e.BookingId== bookingId).FirstOrDefault()?.Total ?? 0;
        }
        private decimal? GetPrePayment(List<BookingPrepayPayment> prepayPayment, string paymentMethodCode)
        {
            return prepayPayment.Where(e => e.BookingMethod.Code.Equals(paymentMethodCode)).Sum(e => e.Total);
        }

     

        public IQueryable<CashBookTransaction> GetTotalRevenueExpenditure(long salonId, string salonBranchIds, string start, string end, string keyword, string action, string cash_book_transaction_category_id)
        {
            return GetCashBookTransaction(salonId, salonBranchIds, start, end, keyword, action, cash_book_transaction_category_id);
        }

        public IQueryable<RevenueExpenditureVM> GetRevenueExpenditure(long salonId, string salonBranchIds, string start, string end, string keyword, string action, string cash_book_transaction_category_id)
        {
            var data = GetCashBookTransaction(salonId, salonBranchIds, start, end, keyword, action, cash_book_transaction_category_id);
            data = data.Include(e => e.Customer);
            data = data.Include(e => e.CashBookTransactionCategory);
            data = data.Include(e => e.PaymentMethod);
            var rs = data.Select(e => new RevenueExpenditureVM
            {
                Date = e.Created.Value,
                Code = e.Code,
                Action = e.Action,
                CashbookId = e.Id,
                Description = e.CashBookTransactionCategory.Name,
                Cashier = e.Action.Equals(CASHBOOKTRANSACTIONACTION.INCOME) ? e.Customer.Name : e.Cashier,
                PaymentMethod = e.PaymentMethod.Name,
                Total = e.Money
            });
            return rs;
        }
    }
}
