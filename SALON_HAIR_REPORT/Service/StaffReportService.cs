﻿using Microsoft.EntityFrameworkCore;
using SALON_HAIR_CORE.Interface;
using SALON_HAIR_ENTITY.Entities;
using SALON_HAIR_REPORT.Interface;
using SALON_HAIR_REPORT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ULTIL_HELPER;

namespace SALON_HAIR_REPORT.Service
{
    public class StaffReportService : IStaffReport
    {
        private salon_hairContext _salon_hairContext;
        private ICommissionArrangement _commissionArrangement;
        public StaffReportService(ICommissionArrangement commissionArrangement,salon_hairContext salon_hairContext)
        {
            _commissionArrangement = commissionArrangement;
            _salon_hairContext = salon_hairContext;
        }
        /// <summary>
        /// Hoa Hồng nhân viên
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBranchIds"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<StaffCommissionTmp> GetCommissionStaff(long salonId, string salonBranchIds = "", string start = "", string end = "")
        {
            var listsalonBranchIds = StringHelper.GetList<long>(salonBranchIds, ',');
            var dataRange = StringHelper.GetDateRangeQuery(start, end);
            //var data = _commissionArrangement.GetAll();
            var data = _salon_hairContext.CommissionArrangement.Where(e => e.SalonId == salonId);
            data = data.Where(e => e.Invoice.Created.Value.Date >= dataRange.Item1.Date && e.Invoice.Created.Value.Date <= dataRange.Item2.Date.Date);
            data = string.IsNullOrEmpty(salonBranchIds) ? data : data.Where(e => listsalonBranchIds.Contains(e.SalonBranchId.Value));
            data = data.Include(e => e.SaleStaff);
            data = data.Include(e => e.ServiceStaff);
            data = data.Where(e => !(e.SaleStaffId == default && e.ServiceStaffId == default));
            var dataProcess = data.ToList();

             var saleCommission =
                from a in dataProcess
                where a.SaleStaff != null
                select new StaffCommissionTmp
                {
                    StaffId = a.SaleStaffId,
                    SaleCommission = a.CommsionValue,
                    StaffName = a.SaleStaff.Name
                };

            var serviceCommission =
                from a in dataProcess
                where a.ServiceStaff != null && a.CommsionServiceValue != null
                select new StaffCommissionTmp
                {
                    StaffId = a.ServiceStaffId,
                    ServiceCommission = a.CommsionServiceValue,
                    StaffName = a.ServiceStaff.Name
                };

            var combineCommision = saleCommission.Union(serviceCommission);

            var result = from a in combineCommision
                         group a by a.StaffId into b
                         select new StaffCommissionTmp
                         {
                             SaleCommission = b.Sum(e => e.SaleCommission),
                             ServiceCommission = b.Sum(e => e.ServiceCommission),
                             StaffName = b.FirstOrDefault()?.StaffName,
                             SumCommission = b.Sum(e => e.SaleCommission) + b.Sum(e => e.ServiceCommission),
                             StaffId = b.Key
                         };
            return result;
        }
        /// <summary>
        /// Chi tiết hoa hồng theo nhân viên
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="staffId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<CommissionStaffDetailVM> GetCommissionStaffDetail(long salonId, long staffId, string start, string end)
        {
          
            var dataRange = StringHelper.GetDateRangeQuery(start, end);
            var data = _commissionArrangement.QueryForGetReport("", start, end, "", salonId);
            data = data.Where(e => e.SaleStaffId == staffId || e.ServiceStaffId == staffId);
            data = data.Include(e => e.Invoice).ThenInclude(e=>e.Customer);
            var xem = data.ToList();
            var listService = data.Where(e=>e.ServiceStaffId==staffId && e.SaleStaffId!=staffId).ToList().Select(e => new CommissionStaffDetailVM {
                InvoiceCode = e.Invoice.Code,
                CustomerName = e.Invoice.Customer?.Name,
                Name = e.ObjectName,
                SaleCommsion = 0,
                ServiceCommsion = e?.CommsionServiceValue??0,
                Time = e.Created,
                InvoiceId = e.InvoiceId
            });
            var listSales= data.Where(e => e.SaleStaffId == staffId && e.ServiceStaffId != staffId).ToList().Select(e => new CommissionStaffDetailVM
            {
                InvoiceCode = e.Invoice.Code,
                CustomerName = e.Invoice.Customer?.Name,
                Name = e.ObjectName,
                SaleCommsion = e?.CommsionValue ?? 0,
                ServiceCommsion =  0,
                Time = e.Created,
                InvoiceId = e.InvoiceId
            });
            var listSaleAndService = data.Where(e => e.SaleStaffId == staffId && e.ServiceStaffId == staffId).ToList().Select(e => new CommissionStaffDetailVM
            {
                InvoiceCode = e.Invoice.Code,
                CustomerName = e.Invoice.Customer?.Name,
                Name = e.ObjectName,
                SaleCommsion = e?.CommsionValue ?? 0,
                ServiceCommsion = e?.CommsionServiceValue ?? 0,
                Time = e.Created,
                InvoiceId = e.InvoiceId
            });
            var rs = listService.Union(listSales).Union(listSaleAndService);
            rs = rs.OrderBy(e => e.InvoiceCode);
            return rs;
        }
    }
}
